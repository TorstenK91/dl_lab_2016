#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 13:01:14 2017

THE supperior network for Deep Q-Learning

@author: schoelst
"""


from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
import tensorflow as tf

def createModel(conf):
    
    img_size = conf.pob_siz * conf.cub_siz

    model = Sequential()
    # convolutional layers
    model.add(Convolution2D(16, 5, 5, border_mode='valid', input_shape=(img_size,img_size,conf.hist_len), subsample=(2,2)))
    model.add(Activation('relu'))
    model.add(Convolution2D(32, 3, 3, border_mode='valid'))
    model.add(Activation('relu'))

    model.add(Flatten())
    # Note: Keras does automatic shape inference.
    model.add(Dense(256))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))

    model.add(Dense(5))
    model.add(Activation('softmax'))

    optimizer = tf.train.AdamOptimizer(learning_rate=1e-6, beta1=0.9, beta2=0.999, epsilon=1e-08, use_locking=False, name='Adam')

    return model, optimizer
    
if __name__ == '__main__':
    print("creating a model now")
    createModel()