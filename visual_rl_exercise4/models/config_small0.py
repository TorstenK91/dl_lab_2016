# -*- coding: utf-8 -*-

"""
Created on Thu Dec  1 15:13:31 2016

This is a template model

@author: schoels
"""

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
import tensorflow as tf

def createModel(opt):


    ## VGG-like convnet, taken from:
    # https://keras.io/getting-started/sequential-model-guide/
    img_size = opt.pob_siz * opt.cub_siz
    model = Sequential()
    # input: 100x100 images with 3 channels -> (3, 100, 100) tensors.
    # this applies 32 convolution filters of size 3x3 each.
    model.add(Convolution2D(32, 3, 3, border_mode='valid', input_shape=(img_size,img_size,opt.hist_len)))
    model.add(Activation('relu'))
    model.add(Convolution2D(32, 3, 3))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Convolution2D(32, 3, 3, border_mode='valid', input_shape=(img_size,img_size,opt.hist_len)))
    model.add(Activation('relu'))
    model.add(Convolution2D(32, 3, 3))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

#    model.add(Convolution2D(64, 3, 3, border_mode='valid'))
#    model.add(Activation('relu'))
#    model.add(Convolution2D(64, 3, 3))
#    model.add(Activation('relu'))
#    model.add(MaxPooling2D(pool_size=(2, 2)))
#    model.add(Dropout(0.25))

    model.add(Flatten())
    # Note: Keras does automatic shape inference.
    model.add(Dense(256))
    model.add(Activation('relu'))
    model.add(Dense(256))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))
    #model.add(Dense(512))
    #model.add(Activation('relu'))
    #model.add(Dropout(0.25))

    model.add(Dense(5))
    model.add(Activation('softmax'))

    optimizer = tf.train.AdamOptimizer(learning_rate=1e-5, beta1=0.9, beta2=0.999, epsilon=1e-08, use_locking=False, name='Adam')

    #optimizer = tf.train.GradientDescentOptimizer(1e-6)


    return model, optimizer