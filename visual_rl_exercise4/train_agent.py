import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from random import randrange
from keras import backend as K


# custom modules
from utils     import *
from simulator import Simulator
from transitionTable import TransitionTable


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NOTE:
# this is a little helper function that calculates the Q error for you
# so that you can easily use it in tensorflow as the loss
# you can copy this into your agent class or use it from here
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Q_loss(Q_s, action_onehot, Q_s_next, best_action_next, reward, terminal, discount=0.99):
    """
    All inputs should be tensorflow variables!
    We use the following notation:
       N : minibatch size
       A : number of actions
    Required inputs:
       Q_s: a NxA matrix containing the Q values for each action in the sampled states.
            This should be the output of your neural network.
            We assume that the network implments a function from the state and outputs the
            Q value for each action, each output thus is Q(s,a) for one action
            (this is easier to implement than adding the action as an additional input to your network)
       action_onehot: a NxA matrix with the one_hot encoded action that was selected in the state
                      (e.g. each row contains only one 1)
       Q_s_next: a NxA matrix containing the Q values for the next states.
       best_action_next: a NxA matrix with the best current action for the next state
       reward: a Nx1 matrix containing the reward for the transition
       terminal: a Nx1 matrix indicating whether the next state was a terminal state
       discount: the discount factor
    """
    # calculate: reward + discount * Q(s', a*),
    # where a* = arg max_a Q(s', a) is the best action for s' (the next state)
    target_q = (1. - terminal) * discount * tf.reduce_sum(best_action_next * Q_s_next, 1, keep_dims=True) + reward
    # NOTE: we insert a stop_gradient() operation since we don't want to change Q_s_next, we only
    #       use it as the target for Q_s
    target_q = tf.stop_gradient(target_q)
    # calculate: Q(s, a) where a is simply the action taken to get from s to s'
    selected_q = tf.reduce_sum(action_onehot * Q_s, 1, keep_dims=True)
    loss = tf.reduce_sum(tf.square(selected_q - target_q))
    return loss



def main(arfv = None):

    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # NOTE:
    # In contrast to your last exercise you DO NOT generate data before training
    # instead the TransitionTable is build up while you are training to make sure
    # that you get some data that corresponds roughly to the current policy
    # of your agent
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # 0. initialization
    model, optimizer, conf = loadConfig(FLAGS.config_path)
    restoreWeights(model, FLAGS.weights)

    sim = Simulator(conf.map_ind, conf.cub_siz, conf.pob_siz, conf.act_num)
    # setup a large transitiontable that is filled during training

    if conf.disp_on:
        win_all = None
        win_pob = None

    epi_step = 0
    nepisodes = 0

    # Training Data
    trans = TransitionTable(conf.state_siz, conf.act_num, conf.hist_len,
                        conf.minibatch_size, conf.maxlen)
    # Validation Data
    trans_val = TransitionTable(conf.state_siz, conf.act_num, conf.hist_len,
                        conf.minibatch_size, conf.maxlen)


    x = tf.placeholder(tf.float32, shape=(conf.minibatch_size, conf.hist_len*conf.state_siz))
    u = tf.placeholder(tf.float32, shape=(conf.minibatch_size, conf.act_num))
    xn = tf.placeholder(tf.float32, shape=(conf.minibatch_size, conf.hist_len*conf.state_siz))
    r = tf.placeholder(tf.float32, shape=(conf.minibatch_size, 1))
    term = tf.placeholder(tf.float32, shape=(conf.minibatch_size, 1))
    Q = model(tf_reshapeData(x, conf,conf.minibatch_size))

    ## currently deep copying Keras model seems to be impossible with tensorflow
    ## backend. We use the (hacky) solution of loading the model twice from file
    if conf.use_target_network:
        tmodel, _, _ = loadConfig(FLAGS.config_path)
        Qn = tmodel(tf_reshapeData(xn, conf,conf.minibatch_size))
        copyWeights(model, tmodel)
        Qn.trainable = False
    else:
        Qn = model(tf_reshapeData(xn, conf, conf.minibatch_size))


    ustar = tf.one_hot(tf.arg_max(Qn,1),5)


    loss = Q_loss(Q, u, Qn, ustar, r, term,discount = conf.discount)


    state = sim.newGame(conf.tgt_y, conf.tgt_x)
    state_with_history = np.zeros((conf.hist_len*conf.frame_frequency, conf.state_siz))
    append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
    next_state_with_history = np.copy(state_with_history)

    #optimizer =
    train_step = optimizer.minimize(loss)

    with tf.Session() as sess:
        #init_op =
        sess.run(tf.initialize_all_variables())
        for step in xrange(conf.steps):
            if state.terminal or epi_step >= conf.early_stop:
                epi_step = 0
                nepisodes += 1
                # reset the game
                state = sim.newGame(conf.tgt_y, conf.tgt_x)
                # and reset the history
                state_with_history[:] = 0
                append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
                next_state_with_history = np.copy(state_with_history)
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # TODO: here you would let your agent take its action
            #       remember
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            epsilon = annealing_schedule(step, conf)
            action = epsilon_greedy_action(state_with_history,model,conf,epsilon)


            action_onehot = trans.one_hot_action(action)
            next_state = sim.step(action)
            epi_step += 1

            # append to history
            append_to_hist(next_state_with_history,
                           rgb2gray(next_state.pob).reshape(conf.state_siz))

            if np.random.rand() < conf.val_fraction:
                # add datapoint to validation data
                trans_val.add(select_frames_from_hist(state_with_history,
                                                      conf.frame_frequency,
                                                      conf.hist_len).reshape(-1),
                              action_onehot,
                              select_frames_from_hist(next_state_with_history,
                                                      conf.frame_frequency,
                                                      conf.hist_len).reshape(-1),
                              next_state.reward,
                              next_state.terminal)
            else:
                # add datapoint to training data
                trans.add(select_frames_from_hist(state_with_history,
                                                  conf.frame_frequency,
                                                  conf.hist_len).reshape(-1),
                          action_onehot,
                          select_frames_from_hist(next_state_with_history,
                                                  conf.frame_frequency,
                                                  conf.hist_len).reshape(-1),
                          next_state.reward,
                          next_state.terminal)

            # mark next state as current state
            state_with_history = np.copy(next_state_with_history)

            state = next_state
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # TODO: here you would train your agent
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


            if (step > conf.init_rollout_length) and np.mod(step,conf.opt_freq) == 0:
                for ii in range(conf.n_update):
                    state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = trans.sample_minibatch()
                    #print(action_batch)
                    feed_dict = {x : state_batch,
                                 u : action_batch,
                                 xn : next_state_batch,
                                 r : reward_batch,
                                 term : terminal_batch,
                                 K.learning_phase(): 1}
                    sess.run(train_step,feed_dict = feed_dict);

            if conf.use_target_network and (step > conf.init_rollout_length) and np.mod(step,conf.target_update_freq) == 0:
                copyWeights(model, tmodel)
                print("Copied weights to target network")

            if (step > conf.init_rollout_length) and (np.mod(step,conf.eval_frequency) == 0):
                # compute train error
                state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = trans.sample_minibatch()
                feed_dict_train = {x : state_batch,
                                   u : action_batch, 
                                   xn : next_state_batch, 
                                   r : reward_batch, 
                                   term : terminal_batch, 
                                   K.learning_phase(): 0}
                train_err = sess.run(loss, feed_dict = feed_dict_train)
                # compute validation error
                state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = trans_val.sample_minibatch()
                feed_dict_val = {x : state_batch,
                                 u : action_batch,
                                 xn : next_state_batch,
                                 r : reward_batch,
                                 term : terminal_batch,
                                 K.learning_phase(): 0}
                val_err = sess.run(loss, feed_dict = feed_dict_val)

                #print computed error
                print(("iteration: {}, train_error: {}, validation error: {}, "
                      + "epsilon: {}").format(step, train_err, val_err, epsilon))


            if (step > 0) and (np.mod(step,conf.save_frequency) == 0):
                if conf.auto_save:
                    filename = conf.auto_save_name + "iteration{}.h5".format(step)
                    autoSaveWeights(model, filename)
                else:
                    # save dialog
                    saveWeights(model)
                    cont = raw_input("Do you want to continue training? [Y/n] ")
                    if cont.lower() == "" or cont.lower()[0] == "y":
                        pass
                    else:
                        break;

            #model.compile(loss=loss, optimizer=optim)


            if conf.disp_on:
                if (np.mod(step,conf.disp_frequency) <= 200) or step < 1000 :

                    if win_all is None:
                        plt.subplot(121)
                        win_all = plt.imshow(state.screen)
                        plt.subplot(122)
                        win_pob = plt.imshow(state.pob)
                    else:
                        win_all.set_data(state.screen)
                        win_pob.set_data(state.pob)
                    plt.pause(conf.disp_interval)
                    plt.draw()
            """

            Here we either mingle with the keras interface for loss functions to
            make a custom loss function for optimization. Or we define a custom optimizer
            using tensorflow and do custom update steps (as done in assignment 2)


            """


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('config_path',help='The path to the configuartion file')
    parser.add_argument(
        '--weights',
        help='Path to pretrained weights file',
        default=None
    )
    parser.add_argument(
        '--visualize_model',
        help=('Set this FLAG to create an image of the model architecture. '
              +'When Flag is set, no training is done! '),
        action = 'store_true',
        default=False
    )
    FLAGS = parser.parse_args()
    main()