#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 11:54:11 2017

@author: schoelst
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from random import randrange
from keras import backend as K

# custom modules
from utils     import *
from simulator import Simulator
from transitionTable import TransitionTable


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NOTE:
# this is a little helper function that calculates the Q error for you
# so that you can easily use it in tensorflow as the loss
# you can copy this into your agent class or use it from here
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Q_loss(Q_s, action_onehot, Q_s_next, best_action_next, reward, terminal, discount=0.99):
    """
    All inputs should be tensorflow variables!
    We use the following notation:
       N : minibatch size
       A : number of actions
    Required inputs:
       Q_s: a NxA matrix containing the Q values for each action in the sampled states.
            This should be the output of your neural network.
            We assume that the network implments a function from the state and outputs the
            Q value for each action, each output thus is Q(s,a) for one action
            (this is easier to implement than adding the action as an additional input to your network)
       action_onehot: a NxA matrix with the one_hot encoded action that was selected in the state
                      (e.g. each row contains only one 1)
       Q_s_next: a NxA matrix containing the Q values for the next states.
       best_action_next: a NxA matrix with the best current action for the next state
       reward: a Nx1 matrix containing the reward for the transition
       terminal: a Nx1 matrix indicating whether the next state was a terminal state
       discount: the discount factor
    """
    # calculate: reward + discount * Q(s', a*),
    # where a* = arg max_a Q(s', a) is the best action for s' (the next state)
    target_q = (1. - terminal) * discount * tf.reduce_sum(best_action_next * Q_s_next, 1, keep_dims=True) + reward
    # NOTE: we insert a stop_gradient() operation since we don't want to change Q_s_next, we only
    #       use it as the target for Q_s
    target_q = tf.stop_gradient(target_q)
    # calculate: Q(s, a) where a is simply the action taken to get from s to s'
    selected_q = tf.reduce_sum(action_onehot * Q_s, 1, keep_dims=True)
    loss = tf.reduce_sum(tf.square(selected_q - target_q))
    return loss


def main(arfv = None):

    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # NOTE:
    # In contrast to your last exercise you DO NOT generate data before training
    # instead the TransitionTable is build up while you are training to make sure
    # that you get some data that corresponds roughly to the current policy
    # of your agent
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # 0. initialization
    model, optimizer, conf = loadConfig(FLAGS.config_path)
    restoreWeights(model, FLAGS.weights)
    
    # This is testing, you want to get visual output
    conf.disp_on = True

    if conf.disp_on:
        win_all = None
        win_pob = None

    n_iterations = 1*10**5
        
    sim = Simulator(conf.map_ind, conf.cub_siz, conf.pob_siz, conf.act_num)

    solved_list = np.zeros((n_iterations,))
    n_steps_list = np.zeros((n_iterations,))
    for it in range(n_iterations):

        state = sim.newGame(conf.tgt_y, conf.tgt_x)
        state_with_history = np.zeros((conf.hist_len*conf.frame_frequency, conf.state_siz))
        append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
        next_state_with_history = np.copy(state_with_history)

        step = 0
        state = sim.newGame(conf.tgt_y, conf.tgt_x)
        while step < conf.early_stop and not state.terminal:

            #get the optimal learned action (max_u Q(state,u))
            action = np.argmax(np.squeeze(model.predict(reshapeSingle(select_frames_from_hist(state_with_history,conf.frame_frequency,conf.hist_len),conf))))
            next_state = sim.step(action)
            step += 1

            append_to_hist(next_state_with_history, rgb2gray(next_state.pob).reshape(conf.state_siz))
            state_with_history = np.copy(next_state_with_history)

            state = next_state

            if conf.disp_on:
                if win_all is None:
                    plt.subplot(121)
                    win_all = plt.imshow(state.screen)
                    plt.subplot(122)
                    win_pob = plt.imshow(state.pob)
                else:
                    win_all.set_data(state.screen)
                    win_pob.set_data(state.pob)
                plt.pause(conf.disp_interval)
                plt.draw()

        if state.terminal:
            solved_list[it] = 1.0
        n_steps_list[it] = step
    
    """Report"""
    avg_steps = np.mean(n_steps_list)
    succ_ratio = np.sum(solved_list) / n_iterations
    print("average number of steps: {}, success ratio: {}".format(avg_steps, succ_ratio))

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('config_path',help='The path to the configuartion file')
    parser.add_argument(
        'weights',
        help='Path to pretrained weights file',
        default=None
    )
    parser.add_argument(
        '--visualize_model',
        help='Set this FLAG to create an image of the model architecture. When Flag is set, no training is done! ',
        action = 'store_true',
        default=False
    )
    FLAGS = parser.parse_args()
    main()