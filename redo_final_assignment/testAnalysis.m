% this script generates plots that make it simple to compare test results

fnames = dir('log/*_results.log');
print_on = true;


for fnum = 1:4:length(fnames)
    fname = fnames(fnum).name;
    basename = strsplit(fname, '_test_results.log');
    basename = basename{1};
    if ~isempty(strfind(fname, '_lin'));
        tit = 'Linear Output Activation';
    else
        tit = 'Softmax Output Activation';
    end

    disp(['----------------' fname '-----------------------']);
    % load csv file
    data = [];
    step = [];
    n_iterations = [];
    tgt_reach_num = [];
    tgt_reach_ratio = [];
    avg_steps = [];
    for i=1:4
        fname = sprintf('%s%i%s',basename(1:end-1),i,'_test_results.log');
        try
            data = csvread(['log' filesep fname]);
        catch
            continue;
        end
        step = [step; data(:,1)./1e3];
        n_iterations = [n_iterations; data(:,2)];
        tgt_reach_num = [tgt_reach_num; data(:,3)];
        tgt_reach_ratio = [tgt_reach_ratio; data(:,4)];
        avg_steps = [avg_steps; data(:,5)];
    end
    %% plotting
    basename = strsplit(fname,'.');
    save_name = strcat('report',filesep,'figures',filesep,'boxplot_',basename(1));
    if print_on
        h = clf(figure(1));
    else
        h = figure;
    end
    set(h, 'Position', pos);
    boxplot(tgt_reach_ratio .* 100,step)
    %title([tit fname]);
    xlabel('Number of Steps [1000 steps]')
    ylabel('Success ratio in [%]')
    xlim([0 11])
    ylim([0 110])
    %set(gca,'XTick',0:10)
    set(gca,'YTick',0:10:100);
    set(gca,'fontsize',font_size)
    if print_on
        fpath = char(strcat(save_name, '_linear.eps'));
        disp(['Saving: ' fpath])
        print(h, fpath, '-depsc');
    end
end
