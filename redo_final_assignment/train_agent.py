#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from random import randrange
from keras import backend as K

# custom modules
from utils     import *
from simulator import Simulator
from transitionTable import TransitionTable, TransitionTableRankPrio


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NOTE:
# this is a little helper function that calculates the Q error for you
# so that you can easily use it in tensorflow as the loss
# you can copy this into your agent class or use it from here
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Q_loss(Q_s, action_onehot, Q_s_next, best_action_next, reward, terminal, discount=0.99):
    """
    All inputs should be tensorflow variables!
    We use the following notation:
       N : minibatch size
       A : number of actions
    Required inputs:
       Q_s: a NxA matrix containing the Q values for each action in the sampled states.
            This should be the output of your neural network.
            We assume that the network implments a function from the state and outputs the
            Q value for each action, each output thus is Q(s,a) for one action
            (this is easier to implement than adding the action as an additional input to your network)
       action_onehot: a NxA matrix with the one_hot encoded action that was selected in the state
                      (e.g. each row contains only one 1)
       Q_s_next: a NxA matrix containing the Q values for the next states.
       best_action_next: a NxA matrix with the best current action for the next state
       reward: a Nx1 matrix containing the reward for the transition
       terminal: a Nx1 matrix indicating whether the next state was a terminal state
       discount: the discount factor
    """
    # calculate: reward + discount * Q(s', a*),
    # where a* = arg max_a Q(s', a) is the best action for s' (the next state)
    target_q = (1. - terminal) * discount * tf.reduce_sum(best_action_next * Q_s_next, 1, keep_dims=True) + reward
    # NOTE: we insert a stop_gradient() operation since we don't want to change Q_s_next, we only
    #       use it as the target for Q_s
    target_q = tf.stop_gradient(target_q)
    # calculate: Q(s, a) where a is simply the action taken to get from s to s'
    selected_q = tf.reduce_sum(action_onehot * Q_s, 1, keep_dims=True)
    loss = tf.reduce_sum(tf.square(selected_q - target_q))
    return loss

def Q_loss_samplewise(Q_s, action_onehot, Q_s_next, best_action_next, reward, terminal, discount=0.99):
    """
    Calculate sample wise Q_loss

    N: size of input

    outputs: Nx1 matrix of Q_losses


    THIS IS NOT SUPPOSED TO BE CALLED FOR GRADIENT OPTIMIZATION
    """
    target_q = (1. - terminal) * discount * tf.reduce_sum(best_action_next * Q_s_next, 1, keep_dims=True) + reward
    # NOTE: we insert a stop_gradient() operation since we don't want to change Q_s_next, we only
    #       use it as the target for Q_s
    #target_q = tf.stop_gradient(target_q)
    # calculate: Q(s, a) where a is simply the action taken to get from s to s'
    selected_q = tf.reduce_sum(action_onehot * Q_s, 1, keep_dims=True)

    return tf.square(selected_q - target_q)


def main(arfv = None):

    # initialize network
    model, optimizer, conf = loadConfig(FLAGS.config_path)
    restoreWeights(model, FLAGS.weights)

    x_dim2 = conf.n_channels*conf.state_siz
    x = tf.placeholder(tf.float32, shape=(conf.minibatch_size, x_dim2))
    u = tf.placeholder(tf.float32, shape=(conf.minibatch_size, conf.act_num))
    xn = tf.placeholder(tf.float32, shape=(conf.minibatch_size, x_dim2))
    r = tf.placeholder(tf.float32, shape=(conf.minibatch_size, 1))
    term = tf.placeholder(tf.float32, shape=(conf.minibatch_size, 1))
    Q = model(tf_reshapeData(x, conf,conf.minibatch_size))

    # create target network
    if conf.use_target_network:
        tmodel, _, _ = loadConfig(FLAGS.config_path)
        Qn = tmodel(tf_reshapeData(xn, conf,conf.minibatch_size))
        copyWeights(model, tmodel)
        Qn.trainable = False
    else:
        Qn = model(tf_reshapeData(xn, conf, conf.minibatch_size))

    ustar = tf.one_hot(tf.arg_max(Qn,1),5)
    loss = Q_loss(Q, u, Qn, ustar, r, term,discount = conf.discount)
    train_step = optimizer.minimize(loss)

    # setup a large transitiontable that is filled during training



    # setup simulation
    sim = Simulator(conf)
    state = sim.newGame(conf.tgt_y, conf.tgt_x)
    state_with_history = np.zeros((conf.hist_len*conf.frame_frequency, conf.state_siz))
    append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
    next_state_with_history = np.copy(state_with_history)

    # initialize plotting if desired
    if conf.disp_on:
        win_all = None
        win_pob = None

    # initialize episode counters
    epi_step = 0
    nepisodes = 0
    n_tgt_batch = 0
    n_epi_batch = 0
    n_steps_batch = 0
    with tf.Session() as sess, openLog(conf) as log_handle:
        sess.run(tf.initialize_all_variables())

        if conf.priority_minibatch_sampling:
            tf_variables = (x,u,xn,r,term,Q)
            loss_samplewise = Q_loss_samplewise(Q, u, Qn, ustar, r, term,discount = conf.discount)
            trans = TransitionTableRankPrio(conf)
        else:
            trans = TransitionTable(conf)
        #init_op =

        for step in xrange(conf.steps):
            # start new episode if agent reached goal or maximum steps
            if state.terminal or epi_step >= conf.early_stop:
                if state.terminal:
                    n_tgt_batch += 1
                    n_steps_batch += epi_step
                epi_step = 0
                nepisodes += 1
                n_epi_batch += 1
                # reset the game
                state = sim.newGame(conf.tgt_y, conf.tgt_x)
                # and reset the history
                state_with_history[:] = 0
                append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
                next_state_with_history = np.copy(state_with_history)

            # let agent take its action
            epsilon = annealing_schedule(step, conf)
            action = epsilon_greedy_action(state_with_history, state, model, conf, epsilon)


            action_onehot = trans.one_hot_action(action)
            next_state = sim.step(action)
            epi_step += 1

            # append to history
            append_to_hist(next_state_with_history,
                           rgb2gray(next_state.pob).reshape(conf.state_siz))

            # add datapoint to training data

            if conf.priority_minibatch_sampling:
                ## calculate Q_loss for single input
                trans.add(select_frames_from_hist(state_with_history,
                                                  state, conf).reshape(-1),
                      action_onehot,
                      select_frames_from_hist(next_state_with_history,
                                              state, conf).reshape(-1),
                      next_state.reward,
                      next_state.terminal,
                      loss_samplewise,
                      tf_variables,
                      sess)
            else:
                trans.add(select_frames_from_hist(state_with_history,
                                              state, conf).reshape(-1),
                      action_onehot,
                      select_frames_from_hist(next_state_with_history,
                                              state, conf).reshape(-1),
                      next_state.reward,
                      next_state.terminal)

            # mark next state as current state
            state_with_history = np.copy(next_state_with_history)
            state = next_state


            # train agent if collected full batch
            if (step > conf.init_rollout_length) and np.mod(step,conf.opt_freq) == 0:
                for ii in range(conf.n_update):
                    if conf.priority_minibatch_sampling:
                        state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = trans.sample_minibatch()
                        #print(action_batch)
                        feed_dict = {x : state_batch,
                                 u : action_batch,
                                 xn : next_state_batch,
                                 r : reward_batch,
                                 term : terminal_batch,
                                 K.learning_phase(): 1}
                        sess.run(train_step,feed_dict = feed_dict);

                        for jj in range(conf.minibatch_size):
                            trans.add(state_batch[jj],
                                  action_batch[jj],
                                  next_state_batch[jj],
                                  reward_batch[jj],
                                  terminal_batch[jj],
                                  loss_samplewise,tf_variables,sess)

                    else:
                        state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = trans.sample_minibatch()
                        #print(action_batch)
                        feed_dict = {x : state_batch,
                                 u : action_batch,
                                 xn : next_state_batch,
                                 r : reward_batch,
                                 term : terminal_batch,
                                 K.learning_phase(): 1}
                        sess.run(train_step,feed_dict = feed_dict);

            if conf.use_target_network and (step > conf.init_rollout_length) and np.mod(step,conf.target_update_freq) == 0:
                copyWeights(model, tmodel)
                print("Copied weights to target network")

            # print some console output
            if (step > conf.init_rollout_length) and (np.mod(step,conf.eval_frequency) == 0):
                # compute train error
                state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = trans.sample_minibatch()
                feed_dict_train = {x : state_batch,
                                   u : action_batch,
                                   xn : next_state_batch,
                                   r : reward_batch,
                                   term : terminal_batch,
                                   K.learning_phase(): 0}
                train_err = sess.run(loss, feed_dict = feed_dict_train)

                #print computed error
                if n_tgt_batch > 0:
                    avg_steps = (1. * n_steps_batch) / (1. * n_tgt_batch)
                    tgt_ratio = 100. * float(n_tgt_batch) / float(n_epi_batch)
                else:
                    avg_steps = 0.
                    tgt_ratio = 0.

                print(("steps: {}, train_error: {}, epsilon: {:.3f}, "
                      + "%|# target reached: {:.1f}|{}, average steps: {:.1f}")
                    .format(step, train_err, epsilon, tgt_ratio, n_tgt_batch, avg_steps))

                # write to log file
                append2log(log_handle, [step, train_err, epsilon,
                                        tgt_ratio, n_tgt_batch, avg_steps])
                n_tgt_batch = 0
                n_steps_batch = 0
                n_epi_batch = 0

            # save current weights
            if (step > 0) and (np.mod(step,conf.save_frequency) == 0):
                if conf.auto_save:
                    filename = conf.auto_save_name + "iteration{}.h5".format(step)
                    autoSaveWeights(model, filename)
                else:
                    # save dialog
                    saveWeights(model)
                    cont = raw_input("Do you want to continue training? [Y/n] ")
                    if cont.lower() == "" or cont.lower()[0] == "y":
                        pass
                    else:
                        break;

            # plot some output if desired
            if conf.disp_on:
                if (np.mod(step,conf.disp_frequency) <= 200) or step < 1000 :

                    if win_all is None:
                        plt.subplot(221)
                        win_all = plt.imshow(state.screen)
                        plt.subplot(223)
                        win_pob = plt.imshow(state.pob)
                        plt.subplot(224)
                        win_tgt = plt.imshow(state.tgt_pob)
                    else:
                        win_all.set_data(state.screen)
                        win_pob.set_data(state.pob)
                        win_tgt.set_data(state.tgt_pob)
                    plt.pause(conf.disp_interval)
                    plt.draw()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('config_path',help='The path to the configuartion file')
    parser.add_argument(
        '--weights',
        help='Path to pretrained weights file',
        default=None
    )
    parser.add_argument(
        '--visualize_model',
        help=('Set this FLAG to create an image of the model architecture. '
              +'When Flag is set, no training is done! '),
        action = 'store_true',
        default=False
    )
    FLAGS = parser.parse_args()
    main()