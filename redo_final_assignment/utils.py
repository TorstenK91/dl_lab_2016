#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
from random import randrange
from numpy.random import multinomial
import tensorflow as tf

from importlib import import_module
from os import mkdir
from os.path import abspath, join, exists, dirname, basename, realpath

WEIGHTS_DIR = "pretrained"
LOG_DIR = "log"

class State: # return tuples made easy
    def __init__(self, action, reward, screen, terminal, pob, tgt_pob):
        self.action   = action
        self.reward   = reward
        self.screen   = screen
        self.terminal = terminal
        self.pob      = pob
        self.tgt_pob  = tgt_pob


# The following functions were taken from scikit-image
# https://github.com/scikit-image/scikit-image/blob/master/skimage/color/colorconv.py

def rgb2gray(rgb):
    if rgb.ndim == 2:
        return np.ascontiguousarray(rgb)

    gray = 0.2125 * rgb[..., 0]
    gray[:] += 0.7154 * rgb[..., 1]
    gray[:] += 0.0721 * rgb[..., 2]

    return gray


def openLog(conf):
    # returns a file handle for the log file
    file_dir = dirname(realpath(__file__))
    fpath = join(file_dir, LOG_DIR, conf.log_file_name)
    fpath = abspath(fpath)
    return open(fpath, 'a')

def append2log(log_handle, data):
    np_data = np.array(data).reshape(1,-1)
    np.savetxt(log_handle, np_data, delimiter=',')

def loadModel(model_path,opt):
    """
    This function loads a model specified in model_path
    """
    model_path = abspath(model_path)
    if not exists(model_path):
        raise(ValueError("The specified model does not exist!"))
    else:
        filename = basename(model_path)
        module_name = filename.split('.')[0]
        module_name = 'models.' + module_name
        model_definition = import_module(module_name)
        return model_definition.createModel(opt)

def loadConfig(conf_path):
    conf_path = abspath(conf_path)
    if not exists(conf_path):
        raise(ValueError("The specified configuration does not exist!"))
    else:
        filename = basename(conf_path)
        module_name = filename.split('.')[0]
        module_name = 'config.' + module_name
        configuration = import_module(module_name)
        conf = configuration.Config()
        conf_dir = dirname(conf_path)
        model_path = join(conf_dir, '..', 'models', conf.model_name)
        model, optimizer = loadModel(model_path, conf)
        return model, optimizer, conf


def restoreWeights(model, weights_path=None, by_name=True):
    """
    This function loads pretrained weights specified in weights_path
    """
    if weights_path:
        weights_path = abspath(weights_path)
        if not exists(weights_path):
            raise(ValueError("The specified weights file does not exist!"))
        else:
            model.load_weights(weights_path, by_name=by_name)
            print("Weights {} restored.".format(weights_path))
    else:
        pass

    return model


def saveWeights(model):
    """
    This function saves the weights of a session into file `weights_name`.
    """

    save = raw_input("Do you want to save the current weights? [Y/n] ")
    if save and save.lower()[0] == "n":
        pass
    else:
        filename = ""
        while not filename:
            filename = raw_input("Please enter a filename: ")
        print("saving weights now...")

        weights_dir = abspath(WEIGHTS_DIR)
        if not exists(weights_dir):
            mkdir(weights_dir)
            print("created {}".format(weights_dir))
        filepath = join(weights_dir, filename)

        model.save_weights(filepath)
        print("The current weights are safely stored at {}".format(filepath))

def autoSaveWeights(model, filename):
    weights_dir = abspath(WEIGHTS_DIR)
    if not exists(weights_dir):
        mkdir(weights_dir)
        print("created {}".format(weights_dir))
    filepath = join(weights_dir, filename)
    model.save_weights(filepath)
    print("The current weights are safely stored at {}".format(filepath))

def copyWeights(sourceModel, targetModel):
    targetModel.set_weights(sourceModel.get_weights())

def reshapeData(data, opt):
    """ reshapes data from (num_datapoints, state_size) to
    (num_datapoints, hist_size, img_size, img_size)"""
    hist_size = opt.hist_len
    num_datapoints = data.shape[0]
    img_size = opt.pob_siz * opt.cub_siz
    assert(data.shape[1] == img_size**2 * hist_size)
    new_shape = [num_datapoints, hist_size, img_size, img_size]
    #print("reshaped data from: {} to: {}".format(data.shape, new_shape))
    data = data.reshape(new_shape)
    return np.rollaxis(data, 1, 4)

def reshapeSingle(data,opt):
    hist_size = opt.n_channels
    img_size = opt.pob_siz * opt.cub_siz
    #assert (data.shape[0] == img_size**2 * hist_size)
    new_shape = [1, hist_size, img_size, img_size]
    data = data.reshape(new_shape)
    return np.transpose(data,(0,2,3,1))



def tf_reshapeData(data, opt,num_datapoints):
    """ reshapes data from (num_datapoints, state_size) to
    (num_datapoints, hist_size, img_size, img_size)"""
    hist_size = opt.n_channels
    #num_datapoints = data.shape[0]
    img_size = opt.pob_siz * opt.cub_siz
    #assert(data.shape[1] == img_size**2 * hist_size)
    new_shape = [num_datapoints, hist_size, img_size, img_size]
    #print("reshaped data from: {} to: {}".format(data.shape, new_shape))
    data = tf.reshape(data,new_shape)
    return tf.transpose(data,(0,2,3,1))

def select_frames_from_hist(hist,state,conf):
    """


    """
    if conf.frame_frequency == 1:
        hist_new = hist
    else:
        n_frames = conf.frame_frequency*conf.hist_len
        index = np.arange(n_frames)[::-1][0:n_frames:conf.frame_frequency]
        hist_new = hist[index,:]

    if conf.target_view:
        tgt = rgb2gray(state.tgt_pob).reshape(conf.state_siz)
        hist_new = np.vstack((hist_new,tgt))

    return hist_new.reshape(-1)


def append_to_hist(state, obs):
    """
    Add observation to the state.
    """
    for i in range(state.shape[0]-1):
        state[i, :] = state[i+1, :]
    state[-1, :] = obs

def softmax(x):
    """Compute softmax values for each sets of scores in x.

    from http://stackoverflow.com/questions/34968722/softmax-function-python
    """
    return np.exp(x) / np.sum(np.exp(x), axis=0)

def _sample_action(softmax_output):
    """
    Sample an action from the softmax output of the CNN

    """

    softmax_norm = softmax_output / (np.sum(softmax_output) + 1e-5)
    action = multinomial(1,softmax_norm)

    return np.argmax(action)

def epsilon_greedy_action(state_with_history,state,model,conf,epsilon):
    """
    Sample a random action with probability epsilon, and
    the optimal action otherwise

    """
    if np.random.rand() < epsilon:
        action = randrange(conf.act_num)
    else:
        hist = select_frames_from_hist(state_with_history,
                                       state,
                                       conf)
        if conf.softmax_sampling:
            softout = softmax(model.predict(reshapeSingle(hist,conf)))
            action = _sample_action(np.squeeze(softout))
        else:
            prediction = model.predict(reshapeSingle(hist,conf))
            action = np.argmax(np.squeeze(prediction))
    return action


def epsilon_softmax_greedy_action(state_with_history,state,model,conf,epsilon):
    """
    Sample a random action with probability epsilon, and
    the optimal action otherwise

    """
    if np.random.rand() < epsilon:
        if conf.softmax_sampling:
            hist = select_frames_from_hist(state_with_history,
                                           state,
                                           conf)
            softout = softmax(model.predict(reshapeSingle(hist,conf)))
            action = _sample_action(np.squeeze(softout))
        else:
            action = randrange(conf.act_num)
    else:
        hist = select_frames_from_hist(state_with_history,
                                       state,
                                       conf)
        prediction = model.predict(reshapeSingle(hist,conf))
        action = np.argmax(np.squeeze(prediction))
    return action


def annealing_schedule(step, conf):
    """
    implements the decay of epsilon over time.

    To test the behavior, just call it with a configuration and
    input different values for step, i.e. 1, conf.n_no_epsilon,
    and (conf.epsilon_decay)**-1
    """
    if step < conf.init_rollout_length:
        # this just gets a random action
        epsilon = 1.0
    elif step > conf.n_no_epsilon:
        epsilon = 0.0
    elif conf.epsilon_greedy <= conf.epsilon_min:
        epsilon = conf.epsilon_min
    else:
        epsilon = (conf.epsilon_greedy - conf.epsilon_min)
        epsilon = epsilon * np.exp(-conf.epsilon_decay*step) + conf.epsilon_min
    return epsilon