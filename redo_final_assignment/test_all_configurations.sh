cd config;
config_names=(sm_base_*.py);
cd ..;
for config_name in "${config_names[@]}"; do
	fname=${config_name%.py}
	echo "Now handeling ${fname}";
	pt_weights=(pretrained/"${fname}"_*.h5);
	for pt_weight in "${pt_weights[@]}"; do
		python test_agent.py config/${config_name} ${pt_weight};
	done
done
