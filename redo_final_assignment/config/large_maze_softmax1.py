#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:44:06 2017

@author: schoelst
"""

from os.path import basename, splitext
from default_config import DefaultConfig

class Config(DefaultConfig):
    map_ind = 0
    output_activation = 1
    init_learning_rate = 1e-6

    relu_factor = 0.
    hist_len = 6
    maxlen = 20*10**4
    steps = 5001*10**3 #total number of steps
    target_update_freq = 4*10**4
    init_rollout_length = maxlen
    epsilon_decay = 1e-6
    epsilon_min = 0.1
    discount = 0.99

    def __init__(self):
        """ Add your changes here"""
        pass

        """ NO Changes beyond this line"""
        conf_name = splitext(basename(__file__))[0]
        print("setup {}".format(conf_name))
        self.auto_save_name = conf_name + "_"
        self.log_file_name = conf_name + ".log"
        self.update()
