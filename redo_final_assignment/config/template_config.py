#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:44:06 2017

@author: schoelst
"""

from os.path import basename, splitext
from default_config import DefaultConfig

class Config(DefaultConfig):
    """ Add your changes Here"""
    pass

    def __init__(self):
        """ You may do stuff here"""
        pass

        """ NO Changes beyond this line"""
        self.auto_save_name = splitext(basename(__file__))[0] + "_"
        self.log_file_name = splitext(basename(__file__))[0] + ".log"
        self.update()
