#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:44:06 2017

@author: schoelst
"""

from os.path import basename, splitext
from default_config import DefaultConfig

class Config(DefaultConfig):
    """ Add your changes Here"""
    map_ind = 0 # 0: large; 1: small
    init_learning_rate = 1e-6 # initial learning rate (AdamOptimizer)
    relu_factor = 0.
    hist_len = 5
    steps = 2001*10**3 #total number of steps
    maxlen = 2e5
    init_rollout_length = maxlen
    target_update_freq = 1e4


    target_update_freq = 1*10**4

    epsilon_decay = 5e-6 # exponential decay factor of the annealing schedule
    epsilon_min = 0.1 # minimum epsilon
    n_no_epsilon = 1e9; # after this iteration set epsilon to 0
    discount = 0.99 # discount factor in the q-updates

    def __init__(self):
        """ You may do stuff here"""
        pass

        """ NO Changes beyond this line"""
        self.auto_save_name = splitext(basename(__file__))[0] + "_"
        self.log_file_name = splitext(basename(__file__))[0] + ".log"
        self.update()