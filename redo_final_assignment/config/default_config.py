#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 13:17:33 2017

@author: schoelst
"""

class DefaultConfig:
    """
    This class replaces the Options class by extending it by several fields and
    turning it into a full configuration that contains all hyper parameters
    for training
    """

    # simulation preferences (output and display)
    disp_on = False
    disp_frequency = 1e4 #plot 100 steps every disp_frequency number of steps
    eval_frequency = 1000 # how often to print the minibatch loss
    map_ind = 1 # 0: large; 1: small
    change_tgt = False


    # network
    model_name = "tnet.py"
    output_activation = 1 # 0: linear, 1: softmax. 2: softsign, else: linear
    init_learning_rate = 1e-7 # initial learning rate (AdamOptimizer)

    """
                       Training Hyper Parameters
    """
    # Tweaking
    relu_factor = 0.01 # .01 is default, 0.0 disables leaky behavior
    target_view = False # adds image of target as additional input (not implemented yet)
    softmax_sampling = False # enables softmax sampling instead of argmax
    priority_minibatch_sampling = True
    uniform_priority_sampling = True
    p_geometric_priosampling = 5e-4 # parameter of geometric distribution sampling

    # History
    hist_len = 4
    frame_frequency = 2 #incorporate only every k-th frame into state
    n_channels = hist_len + target_view

    # data and processing
    minibatch_size  = 32
    maxlen = 10*10**4 #length of the history to be used for training
    steps = 5001*10**3 #total number of steps

    # update and optimization
    opt_freq = 1*10**3 # optimize every k-th step of applying the current policy to the system
    n_update = 1*10**3 # how many optimization steps afer every opt_freq steps on the system
    # Target network
    use_target_network = True # enable/ disable target network
    target_update_freq = 2*10**4 # update target network (copy weights) every n_target steps


    # Saving
    save_frequency = 2e5 # num iterations to between two prompts for saving
    auto_save = True #automatically save after save_frequency number of steps
    auto_save_name = None
    log_file_name = None


    # epsilon
    init_rollout_length = maxlen #how many (uniformly) random steps before starting to optimize
    epsilon_greedy = 1.0 # initial epsilon value
    epsilon_decay = 1e-6 # exponential decay factor of the annealing schedule
    epsilon_min = 0.1 # minimum epsilon
    n_no_epsilon = 2e9; # after this iteration set epsilon to 0

    # Q-learning
    discount = 0.95 # discount factor in the q-updates
    terminal_reward  = 100. # reward for reaching the goal
    step_reward      = -0.1 # reward for making a step
    collision_reward = -1.  # reward for running into a wall

    """ static definitions form options """
    states_fil = "states.csv"
    labels_fil = "labels.csv"
    network_fil = "network.json"
    weights_fil = "network.h5"
    # simulator config
    disp_interval = .005
    if map_ind == 0:
        cub_siz = 5
        pob_siz = 5 # for partial observation
        tgt_pob_siz = 5 # size of target pob
        # this defines the goal position
        tgt_y = 12
        tgt_x = 11
        early_stop = 100
    elif map_ind == 1:
        cub_siz = 10
        pob_siz = 3 # for partial observation
        tgt_pob_siz = 3 # size of target pob
        # this defines the goal position
        tgt_y = 5
        tgt_x = 5
        early_stop = 75
    state_siz = (pob_siz * cub_siz) ** 2 # when use pob as input
    if change_tgt:
        tgt_y = None
        tgt_x = None
    act_num = 5

    eval_nepisodes  = 10

    def update(self):
        """update parts of the configuration that depend on others"""

        self.n_channels = self.hist_len + self.target_view

        # simulator config
        if self.map_ind == 0:
            self.cub_siz = 5
            self.pob_siz = 5 # for partial observation
            self.tgt_pob_siz = 5 # size of target pob
            # this defines the goal position
            self.tgt_y = 12
            self.tgt_x = 11
            self.early_stop = 100
        elif self.map_ind == 1:
            self.cub_siz = 10
            self.pob_siz = 3 # for partial observation
            self.tgt_pob_siz = 3 # size of target pob
            # this defines the goal position
            self.tgt_y = 5
            self.tgt_x = 5
            self.early_stop = 75
        self.state_siz = (self.pob_siz * self.cub_siz) ** 2 # when use pob as input
        if self.change_tgt:
            self.tgt_y = None
            self.tgt_x = None