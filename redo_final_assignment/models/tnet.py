#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 13:01:14 2017

THE supperior network for Deep Q-Learning

@author: schoelst
"""


from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Convolution2D
from keras.layers.advanced_activations import LeakyReLU
from keras import backend as K
import tensorflow as tf

def log(x):
	return K.log(0.1 + x)

def createModel(conf):

    img_size = conf.pob_siz * conf.cub_siz

    model = Sequential()
    # convolutional layers
    model.add(Convolution2D(16, 5, 5, border_mode='valid',
                            input_shape=(img_size,img_size,conf.n_channels),
                            subsample=(2,2)))
    model.add(Activation('relu'))
    model.add(Convolution2D(32, 3, 3, border_mode='valid'))
    model.add(Activation('relu'))

    model.add(Flatten())
    # Note: Keras does automatic shape inference.
    model.add(Dense(256))
    model.add(LeakyReLU(alpha=conf.relu_factor))
    model.add(Dropout(0.25))

    model.add(Dense(5))
    if conf.output_activation == 0:
        # linear output (-inf, inf)
        pass
    elif conf.output_activation == 1:
        # softmax activation (0,1)
        model.add(Activation('softmax'))
    elif conf.output_activation == 2:
        # softsign actiavation (-1, 1)
        model.add(Activation('softsign'))
    elif conf.output_activation == 3:
        model.add(Activation('softmax'))
        model.add(Activation(log))
        model.add(Dense(5))
    elif conf.output_activation == 4:
        model.add(Activation('softmax'))
        model.add(Dense(5))
    else:
        print("Invalid output_activation, using linear activation")
        # linear output (-inf, inf)
        pass

    optimizer = tf.train.AdamOptimizer(learning_rate=conf.init_learning_rate,
                                       beta1=0.9,
                                       beta2=0.999,
                                       epsilon=1e-08,
                                       use_locking=False,
                                       name='Adam')

    return model, optimizer

if __name__ == '__main__':
    print("creating a model now")
    createModel()
