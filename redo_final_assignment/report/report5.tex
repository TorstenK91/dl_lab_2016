\documentclass[twoside,twocolumn,10pt]{article}

\usepackage{blindtext} % Package to generate dummy text throughout this template 

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[english]{babel} % Language hyphenation and typographical rules

\usepackage[hmarginratio=1:1,top=20mm,left=15mm,right=15mm,bottom=15mm,columnsep=20pt]{geometry} % Document margins
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{ subfig}
\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text
%\usepackage{natbib}
\usepackage{pdfpages}
\usepackage{epstopdf}
\usepackage{enumitem} % Customized lists
\setlist[itemize]{noitemsep} % Make itemize lists more compact

\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\roman{subsection}} % roman numerals for subsections
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles

\usepackage{fancyhdr} % Headers and footers
\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{Deep Learning Lab $\bullet$ 2016/17 $\bullet$ Tobias Sch\"ols, Torsten Koller} % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{titling} % Customizing the title section

\usepackage{hyperref} % For hyperlinks in the PDF

% additional packages
\usepackage{amsmath,amssymb}
%\usepackage{empheq}
\usepackage{graphicx}

% custom definitions
\makeatletter
\newcommand{\pushright}[1]{\ifmeasuring@#1\else\omit\hfill$\displaystyle#1$\fi\ignorespaces}
\newcommand{\pushleft}[1]{\ifmeasuring@#1\else\omit$\displaystyle#1$\hfill\fi\ignorespaces}
\makeatother

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
\posttitle{\end{center}} % Article title closing formatting
\title{Deep Learning Lab: Assignment 5} % Article title
\author{Tobias Sch\"ols, Torsten Koller%
%\textsc{John Smith}\thanks{A thank you or further information} \\[1ex] % Your name
%\normalsize University of California \\ % Your institution
%\normalsize \href{mailto:john@smith.com}{john@smith.com} % Your email address
%\and % Uncomment if 2 authors are required, duplicate these 4 lines if more
%\textsc{Jane Smith}\thanks{Corresponding author} \\[1ex] % Second author's name
%\normalsize University of Utah \\ % Second author's institution
%\normalsize \href{mailto:jane@smith.com}{jane@smith.com} % Second author's email address
}
\date{\today} % Leave empty to omit a date


%-----------------------------------------------------------------
\begin{document}
\maketitle

\begin{abstract}
\noindent 
In this assignment, we refined and extended the Deep Q-learning (DQN) approach presented in the last report. With the approaches presented in this report we were able to train agents which can find a target at a random location from all possible starting positions with a reasonable number of steps. 
Moreover, we implemented and compared different extensions to the previous approach and examined their effect on performance and training behavior.
\end{abstract}

% Print the title
\section{Introduction}
Learning to solve tasks in complex environments only based on visual inputs and a reward signal has long been infeasible due to the high dimensionality of the
state-space and the difficulty of learning meaningful features in image data. The recent success in applying convolutional neural networks (CNN) to computer vision related tasks gave rise to novel approaches in the field of reinforcement learning based on visual input. The recently proposed DQN \cite{mnih2013playing} famously solved all Atari games to roughly human level performance based on visual input only. We apply a similar approach to solving a maze game based on partial view of the maze and show that the DQN approach can indeed solve the given task.

\section{Deep Q-learning with $\epsilon$-greedy exploration}
Q-learning is a model-free approach for learning an optimal policy for a Markov Decision Process (MDP) and is known to provably converge under mild assumptions. This approach uses a reward scheme and a so called Q-table, which stores the value for taking a specific action in a given state. Therefore the memory requirements for storing the Q-table is in $\mathcal{O}(N_\mathrm{states} \cdot N_\mathrm{actions})$. In most applications storing this table is impossible because of its size. One way to overcome this problem is a Deep Q-Learning Network (DQN), that approximates the Q-table through a Convolutional Neural Network (CNN).\\
As proposed in \cite{mnih2013playing} and shown in our previous assignment \cite{kollerSchoels2017dllab} using the same $\epsilon$-greedy exploration scheme supports the training process. We started out with parameters of the $\epsilon$-greedy exploration scheme set to the ones proposed in \cite{kollerSchoels2017dllab}.

\section{Network architecture}
In our previous report \cite{kollerSchoels2017dllab} we examined several network architectures and found a sweet spot for the network size. Therefore we did not investigate further on this behalf, but used the TneT-architecture that was proposed in \cite{kollerSchoels2017dllab}.
TneT consists of three layers, two convolutional layers followed by one fully connected layer. The first convolution layer has 16 hidden units with a $5 \times 5$ kernel and uses a stride of $(2,2)$. The second convolutional layer consists of 32 hidden units with a $3 \times 3$ kernel and stride of $(1,1)$. The fully connected layer has 256 hidden units with a dropout of $0.5$. All layers use ReLU output activations.
\subsubsection*{Modifications}
Only minor modifications were made to the architecture described above. The dropout in the fully connected layer was reduced to $0.25$ and softmax output activation was tested and evaluated as alternative to linear output acitvation. Moreover we replaced the ReLU activation between fully connected and output layer with a \textit{Leaky ReLU} activation. This is parameterized by a factor $\alpha$, that is the steepness of the activation curve for negative inputs. Please note, choosing  $\alpha=0.0$ turns a Leaky ReLU into a standard ReLU activation.

\section{Extensions}\label{extensions}
\subsection{Target Network and Delayed Update}
We built our implementation on top of the one used in \cite{kollerSchoels2017dllab}. This includes a so called target network. Instead of a single network, two decoupled networks are used, the Q-network (used as $Q$) and the target network (used as $Q_{n}$). To avoid stalling or looping behavior in the training progress the weights of the $Q_n$ network were kept fixed and only updated by copying the weights for the $Q$ network after a certain number of steps \cite{mnih2015human}.\\
To further tackle the "moving target" problem, we gathered a number of steps in the system, while keeping the $Q$-network fixed as well and optimizing it afterwards. This scheme was iterated for a given number of steps.

\subsection{Strided Histroy}\label{strided_history}
The frames represent a limited view of the surrounding of the agent with 2 neighboring fields in every direction, resulting in a $5 \times 5$ for segment of the entire map for the large maze and a $3 \times 3$ segment for the small maze. This is rendered into a $25 \times 25$ pixel image. On top of the current frame the agent has access to a set of previous frames, called histroy. Hence, the state is composed of the current image and a history of previously reached positions. Since the maze is only partial observed in every step, incorporating the history of previously seen states into the current state is a common approach to get states which have the Markov property (in dynamical systems, this approach is typically referred to as "(N)ARX" modeling).\\
To have a more efficient and dense representation of the current state of the agent, we altered the implementation to included only every $k^{th}$ frame of previous perspectives (history) into the state. This can be understood as a stride of $k$ on the set of previous frames. 

\subsection{Target View} \label{target_view}
In a scenario with changing target positions, the agents decision have to rely on both, the current state and position of the target to be reached. To provide this information to the Q-learning setting we augment the state with a partial view of the target with the same frame size as the state observations. Our experiments show that adding the target view to the state slows down training considerably, but is crucial for scenarios with changing target locations.\\

\subsection{Softmax Sampling}
Since the network returns a Q-value for every possible action in a given state, we tried exploiting this information by replacing the standard approach of applying the greedy policy $[\pi(s) = \arg\underset{u}{\max} \,Q(s,u)]$ action $u$ with the maximum Q-value with an approach that takes into account the different Q-values. The key idea is to interpret the Q-values for the different actions as a distribution and then sampling from this distribution. There are multiple ways to turn action values into a distribution, we chose to use the softmax function $\left( \phi(u_i) = \, ^{e^{u_i}}\!/ _{\sum_j e^{u_j}} \right)$. That way all probabilities are guaranteed to be positive and to sum to one.\\
Sampling from the softmax of the outputs is what we call \textit{softmax sampling} and it can be used in two ways. First it can be used all the time, such that every action is chosen by softmax sampling. Alternatively it can be used with a certain probability. For the second approach we used the same $\epsilon$ as used in the $\epsilon$-greedy exploration scheme as randomization probabilty and replaced the uniform distribution with the distribution generated by taking the softmax of the output.\\ 
While the use of softmax sampling didn't improve the training process, some considerable improvements could be obtained when doing softmax sampling during test time.

\subsection{Q-loss Based Priority Sampling}\label{priosampling}
The number of samples that can be stored is very limited, therefore it is important to keep mostly samples which have a lot of learning potential, i.e. transitions the network has not yet adapted to. One way to measure this is the Q-loss; a large Q-loss for a given transitions means that the network has not yet learned this behavior. Therefore we implemented the Q-loss based priority sampling proposed in \cite{mnih2013playing, mnih2015human,DBLP:journals/corr/SchaulQAS15}.
When using this method the Q-loss of every sample is stored along with the sample. For performance reasons all samples are then stored in a binary heap that is sorted by the according Q-loss. (A second implementation using a Red-Black Tree showed similar performance but was suffering from memory leakage.)\\
In order to obtain the Q-losses, all samples are propagated through the network before adding them to a temporary Q-table. When samples are pulled out for training, they are removed from the Q-table and added again with the updated Q-loss after training, in \cite{mnih2013playing} this is called \textit{experience replay memory}.\\
We implemented two approaches for sampling minibatches from the TransitionTable. These differ only in the probability distribution used for choosing samples, but show very different performance in practice.

\subsubsection{Geometric Distribution}
When using a geometric distribution for choosing samples, a higher probability is given to samples with a high Q-loss, since the Q-losses are sorted in descending order. The intention is, that the network is presented mainly samples which are not yet performing good on and focuses on these cases. In practice this approach is performing worse than a FIFO buffer. One reason might be, that the network is overfitting on corner cases and therefore hurting the overall performance.

\subsubsection{Uniform Distribution}
%\label{unif_vs_unifprio_vs_geoprio_sampling}
When using geometric sampling the average Q-loss of the drawn samples is significantly higher than the average Q-loss of the table. The authors of \cite{DBLP:journals/corr/SchaulQAS15} suggest that every mini-batch should represent the knowledgebase and therefore have an average Q-loss that is close to the average Q-loss of the entire dataset.\\
Since our dataset is more or less sorted, we made the approach of choosing samples by drawing indexes form a uniform distribution. That way the expectation of the average Q-loss of a mini-batch is equal to the average Q-loss of the dataset. Please note, that depending on the size of the minibatch, the variance of the average Q-loss can be quite high.

\subsection{Output Activation}\label{ouput_activation}
We tested two output activation functions. The first being the standard, linear output activation, the other being a softmax output activation. The linear output has the problem, that the output is not bounded and therefore the output values may explode.\\
When using a softmax output activation the output values are bounded between 0 and 1, providing a regularization mechanism for the Q-network. Thus only values in this range can be represented, which should be considered when designing the reward scheme. Rewards that exceed the range of the output function lead to high Q-losses and strong gradients, and therefore influence the learning behavior.\\
Because of the strict bounds that the softmax function imposes on the output, the final Q-loss is very high for the reward scheme we used. Because of the remarkable effects of the softmax output we investigated its the influence further. Therefore we made two further extensions. First we added an additional fully connected layer after the softmax. Second we applied a $\log$ on the softmax output and then added a fully connected layer. Both of these extensions allow for small final Q-loss errors. Further details about the effects of the new extensions are discussed in the experiments section. \\


\input{experiments}


\section{Code}
As in all previous assignment, the code and detailed information about the conducted experiments as well as results can be found in our bitbucket repository \footnote{See, \url{https://bitbucket.org/TorstenK91/dl_lab_2016} }

\section{Summary and Outlook}
We successfully trained agents to solve maze games of varying difficulty using a Deep Q-learning approach. By applying a number of extensions to guide the training prprogress, we could significantly improve over the results reported in the previous assignment. Still, solving the large maze for single and multiple targets remains an open problem which could be tackled in future research using approaches like Double-Q-learning and other advanced techniques.

\section*{Acknowledgement}
We would like thank our tutors, espcially Tobias Springenberger, for their very valuable input and way leading discussions. Without these these we would not have reached the point we are at now.


\bibliography{handin}
\bibliographystyle{ieeetr}  
%\bibliographystyle{humannat}

\end{document}
