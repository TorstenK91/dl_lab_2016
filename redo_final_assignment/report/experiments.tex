\section{Experiments}
In this section, we investigate the general performance of our DQN-framework as well as the influence of the proposed extensions on the overall performance of our solution. We conducted experiments on the small maze with multiple targets as well as the large maze with a single target. As a baseline for comparison between the different approaches, we take the results obtained from the single target small maze case of the last assignment.

\subsection{Single Target, Small Maze and Evaluation of Proposed Extensions}
We solve the single target, small maze task of the previous assignment using a variety of extensions described in section \ref{extensions}. From now on we will refer to the approach proposed in \cite{kollerSchoels2017dllab} as the "base scenario", with all changes in parameters and extensions being based on it.

\subsubsection*{Softmax Output vs Linear Output}
In our first experiment, the softmax network output is compared with the linear output (fully connected layer) using the parameters and training procedure of the base scenario over $10^6$ iterations. We save the weights obtained after every 200 thousand iterations and apply the corresponding Q-networks to the maze over $10^5$ episodes for each agent. This procedure is repeated four times for each output function and the results are visualized in the box plots shown in figure \ref{box_sm_lin_vs_softmax}. To visualize the training progress of both methods, the success ratio and the average number of required steps to reach the goal of every 1000 steps over the training procedure is shown in figure \ref{fig_sm_linvs_softmax}. 
Both, the training curves and the results obtained during test time clearly indicate that the softmax output stabilizes the training and achieves superior overall performance. One effect which is commonly observed when training Q-networks is that after a phase of increasing training performance, the learning may diverge again. This effect seems to vanish when using the softmax output for the network. \\[1em]
To combine the regularizing effect of the softmax output and the unboundedness of the linear output, we keep the softmax output but append a second fully connected linear output layer, i.e. we obtain a $[ \text{conv2d}-\text{conv2d}- \text{fc1} -\text{fc}_{\text{out}} -\text{softmax}- \text{fc}_{\text{out}}]$ architecture, with 5 hidden units for each $\text{fc}_{\text{out}}$ layer. The training visualization in figure \ref{softmax_fc_training} suggests that there is a stabilizing effect on the learning process, but the pure softmax output still outperforms this hybrid approach. That is especially surprising since given the design of the rewards (+100 for reaching the goal), the Q-loss of the pure softmax network can never converge to zero, but the hybrid approach can due to its unboundedness.
We observed that the softmax output function has a polarizing effect on the output values, i.e. one output is assigned to $\approx 1$ all others are $\approx 0$. By applying the $\log$ the output is expanded again and the polarization is reduced. The preliminary results obtained from training the new log-output function show comparable performance but no significant improvement over the other tested output activations. The training progress of the log output is shown in figure \ref{log_output}

\begin{figure}%

\includegraphics[width=0.45\textwidth]{figures/sm_base_additional_fc_layer1_ratio}
\caption{Training progress of the hybrid architecture with a regularizing softmax appended by a fully connected output layer.}
\label{softmax_fc_training}
\end{figure}

\begin{figure}

\includegraphics[width=0.45\textwidth]{figures/sm_base_log_softmax4_ratio}
\caption{Training progress of the log-softmax output in terms of success ratio.}
\label{log_output}
\end{figure}

\begin{figure}

\centering
\subfloat[Linear Output]{{\includegraphics[width=0.45\textwidth]{figures/boxplot_sm_base_lin_unifprio4_test_results_linear}}}
\\
\subfloat[Softmax Output]{{\includegraphics[trim={4cm 0 0 0},clip,width=0.45\textwidth]{figures/boxplot_sm_base_softmax_unifprio4_test_results_linear}}}
\caption{Boxplots of the test performance of agents after every 200 thousand steps of the training procedure. For each output function, four agents were trained to visualize the stability of the approaches. The results clearly show that the softmax output shows near perfect performance after roughly 400 thousand training steps.}
\label{box_sm_lin_vs_softmax}
\end{figure}



%
% Show the matlab plots of success ratio and avg number of steps here for softmax and linear outputs
%
\begin{figure}%

    \centering
    \subfloat[\,]{{\includegraphics[width=0.45\textwidth]{figures/sm_base_lin_unifprio2_ratio}}}%
    \\
     \subfloat[Linear Output]{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_lin_unifprio2_avg-steps}}}%
    \\
    \subfloat[\,]{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_softmax_unifprio1_ratio}}}%
    \\\subfloat[Softmax Output]{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_softmax_unifprio1_avg-steps}}}%
    
    
\caption{Evolution of the success ratio and the number of required steps to reach targets over the whole training procedure for both, the linear output and the softmax output. We note that due to the annealing of the $\epsilon$ value during training, both the randomization of the agent and learning process is reflected in these graphs.}
\label{fig_sm_linvs_softmax}
\end{figure}



\paragraph{Uniform vs Priority Sampling}
We investigate how the Priority Sampling approaches of section \ref{priosampling} compare to the baseline scenario, where a sliding window over the most recently obtained transitions are stored. In figure \ref{unif_vs_unifprio_vs_geoprio_sampling}, we visualize the evolution of the success ratio and Q-loss during training. %To evaluate the test performance of the agents,  each agent is again applied to the system every $200$ thousand training steps . Each sampling scheme is trained four times and the resulting performance is visualized in the box plots shown in figure \ref{boxplots_noprio_unifprio_geoprio}.

\begin{figure}


\subfloat[\,]{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_lin_unifprio3_ratio}}}%
\\
\subfloat[Uniform Priority Sampling]{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_lin_unifprio2_Q-loss}}}%
\\
\subfloat{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_lin_geoprio3_ratio}}}%
\\
\subfloat[Geometric Priority Sampling]{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_lin_geoprio4_Q-loss}}}%
\\
\subfloat{{\includegraphics[width=0.45\textwidth ]{figures/sm_base_lin_nopriosampling3_ratio}}}%
\\
\subfloat[No Priority Sampling]{{\includegraphics[width=0.45\textwidth]{figures/sm_base_lin_nopriosampling2_Q-loss}}}%
\\

%

%

\caption{Training progress of the agents in terms of Q-loss success ratio for training with three different experience replay schemes; uniform priority sampling (top), priority sampling according to a geometric distribution (middle) and uniform sampling without any prioritization (bottom) }
\label{unif_vs_unifprio_vs_geoprio_sampling}
\end{figure}

%\begin{figure}
% \label{boxplots_noprio_unifprio_geoprio}
%
%
%%
%
%%
%\centering
%    \subfloat[Uniform Priority Sampling]{{\includegraphics[width=0.45\textwidth]{figures/boxplot_sm_base_lin_unifprio4_test_results_linear}}}%
%    \\
%    \subfloat[Geometric Priority Sampling]{{\includegraphics[width=0.45\textwidth]{figures/boxplot_sm_base_lin_geoprio4_test_results_linear}}}%
%    \\
%    \subfloat[No Priority Sampling]{{\includegraphics[width=0.45\textwidth]{figures/boxplot_sm_base_lin_nopriosampling4_test_results_linear}}}%
%   
%\caption{Boxplots of the agents test performance when trained for 2 million steps. The agents were trained using 3 different experience sampling approaches; uniform priority sampling (top), priority sampling according to a geometric distribution (middle) and uniform sampling without any prioritization (bottom). The agent is evaluated every 200.000 training steps with each evaluation consisting of 1000 restarts of the maze.}
%\end{figure}

\subsection{Small Maze, Multiple Targets}
In this task, we train an agent to reach arbitrary goal positions from all possible starting positions in the small maze. We apply the method described in section \ref{target_view} to incorporate the goal information into the training process. For the training procedure we use the AdamOptimizer with a learning rate of $10^{-6}$ and all other parameters fixed to default. We use a strided history (see subsection \ref{strided_history}) of stride 2 with a history of length 3. \newline
As in the previous assignment, our best results were obtained using a altered reward scheme of $+100$ for reaching the goal, $-1$ for moving into a wall and $-0.1$ for all other transitions. We again compare the results obtained when using a softmax output and a linear output for the Q-network. After training both network designs over 2 million iterations, the best performance was achieved using the linear output with uniform priority sampling a success ratio of $78.62 \% $ and an average number of steps of $37.03$. The training progress of both methods is shown in figure \ref{sm_mtgt_outputs}.  


\begin{figure}





\includegraphics[width=0.45\textwidth]{figures/sm_mtgt_lin_unifprio_refine_second_it__ratio}
%

%

\caption{Training progress of the success ratio of an agent trained with a linear output function and uniform priority sampling. The training progress suggests that the results could be further improved by incerasing the number of training iterations.}
\label{sm_mtgt_outputs}
\end{figure}



% Our overall best result was obtained using the ??? output activation. A comparison in terms of average number of steps and success ratio between the different is given in table \ref{sm_mtgt_outputs}.
\subsection{Large Maze, Single Target}
We apply the proposed extensions to solving the task of reaching a fixed goal position from arbitrary starting positions in the large maze. This task is significantly harder than reaching a single or multiple goal positions in the small maze due to the delayed reward for reaching the goal and due to the increase in ambiguity of the states. We tackle the latter problem by increasing the history of visited states to 6, which seemed to provide a sweetspot between increased input complexity and information content in the state. The history stride is kept fixed at 2. Since we don't know of a principle solution to the problem of delayed rewards (the same difficulty is reported in \cite{mnih2013playing}), we can only hope for a gradually increasing performance over a large number of iterations. Hence, agents are trained over 5 Million iterations with the uniform priority sampling method. Again, the softmax and the uniform activation were tested, where this time the goal oriented softmax approach showed worse performance than the linear output. This may be the case because the softmax output overfits goal states too agressively (see section \ref{output_activation} for a short discussion).  Figure \ref{lm_softmax_uniform} compares the training progress of both methods. 


\begin{figure}


 \subfloat[Softmax Output]{{\includegraphics[width=0.45\textwidth]{figures/large_maze_softmax1_ratio}}}%
    \\
    \subfloat[Uniform Output]{{\includegraphics[width=0.45\textwidth]{figures/large_maze1_ratio}}}%
%

%

\caption{Progress of training agents on the large maze in terms of success ratio. The graph for the uniform output suggest that the training has not converged yet and increasing the number of training steps may further improve results.}
\label{lm_softmax_uniform}
\end{figure}
%The network was trained for one million steps using an Adam Optimizer with an initial learning rate of $10^{-6}$, all other parameters were kept at their default values. The first 100 thousand steps were performed using a uniformly distributed random walk. After that we set $\epsilon$ to $0.6$ and exponentially annealed it, reaching $0.01$ after one million steps. This annealing schedule was inspired by \cite{appiahplaying}. \\
%The history frame rate $k$ was chosen to 2 with a history length of 3 frames. This results in a virtual history length of 6.
%We altered the reward scheme to encurage a faster convergence by setting the reward for the goal state to $+~100$, for a step to $-~0.1$ and for a collision (step into a barrier) to $-1$.\\
%The networks were not only benchmarked for ratio of solved scenarios but also for average number of steps. The results were compared against the A-star path planning algorithm from the last assignment.
%With an average number of steps of $6.3724$ and a success ratio of $100\%$ TneT performs only sligtly less efficient than the optimal solution, A-star, taking $5.0739$ steps on average. These results were obtained by simulating 200 thousand scenarios.\\
%Interestingly we observed the best performance after training for 600 thousand steps, afterwards the performance decreased again and the training seemed to diverge. %

