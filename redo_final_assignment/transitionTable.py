#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import heapq
import numpy as np
from random import randrange
from copy import copy
from keras import backend as K

class TransitionTable:

    # basic funcs

    def __init__(self, conf):
        self.state_siz = conf.state_siz
        self.act_num = conf.act_num
        self.hist_len  = conf.n_channels
        self.batch_size = conf.minibatch_size
        self.max_transitions = conf.maxlen

        # memory for state transitions
        max_size = self.max_transitions
        self.states  = np.zeros((max_size, self.state_siz*self.hist_len))
        self.actions = np.zeros((max_size, self.act_num))
        self.next_states = np.zeros((max_size, self.state_siz*self.hist_len))
        self.rewards = np.zeros((max_size, 1))
        self.terminal = np.zeros((max_size, 1))
        self.top = 0
        self.bottom = 0
        self.size = 0

    # helper funcs
    def add(self, state, action, next_state, reward, terminal):
        self.states[self.top] = state
        self.actions[self.top] = action
        self.next_states[self.top] = next_state
        self.rewards[self.top] = reward
        self.terminal[self.top] = terminal
        if self.size == self.max_transitions:
            self.bottom = (self.bottom + 1) % self.max_transitions
        else:
            self.size += 1
        self.top = (self.top + 1) % self.max_transitions

    def one_hot_action(self, actions):
        actions = np.atleast_2d(actions)
        one_hot_actions = np.zeros((actions.shape[0], self.act_num))
        for i in range(len(actions)):
            one_hot_actions[i, int(actions[i])] = 1
        return one_hot_actions

    def sample_minibatch(self, batch_size=None):
        if batch_size is None:
            batch_size = self.batch_size
        state      = np.zeros((self.batch_size, self.state_siz*self.hist_len), dtype=np.float32)
        action     = np.zeros((self.batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((self.batch_size, self.state_siz*self.hist_len), dtype=np.float32)
        reward     = np.zeros((self.batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((self.batch_size, 1), dtype=np.float32)
        for i in range(batch_size):
            index = np.random.randint(self.bottom, self.bottom + self.size)
            state[i]         = self.states.take(index, axis=0, mode='wrap')
            action[i]        = self.actions.take(index, axis=0, mode='wrap')
            next_state[i]    = self.next_states.take(index, axis=0, mode='wrap')
            reward[i]        = self.rewards.take(index, axis=0, mode='wrap')
            terminal[i]      = self.terminal.take(index, axis=0, mode='wrap')
        return state, action, next_state, reward, terminal

    def get_minibatch(self,batch_size = None):
        """
        deterministic version of sample_minibatch, where we get the newest minibatch
        """
        if batch_size is None:
            batch_size = self.batch_size

        #check if enough samples in Table
        if batch_size > self.size:
            raise ValueError('Trying to get more samples than currently available in minibatch')

        # if we want the whole transitiontable, just return the whole transition table
        if batch_size == self.max_transitions:
            return self.states,self.actions,self.next_states,self.rewards,self.terminal

        #get the newest batch
        state      = np.zeros((self.batch_size, self.state_siz*self.hist_len), dtype=np.float32)
        action     = np.zeros((self.batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((self.batch_size, self.state_siz*self.hist_len), dtype=np.float32)
        reward     = np.zeros((self.batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((self.batch_size, 1), dtype=np.float32)
        for i in range(batch_size):
            index = self.top - (i+1)
            state[i]         = self.states.take(index, axis=0, mode='wrap')
            action[i]        = self.actions.take(index, axis=0, mode='wrap')
            next_state[i]    = self.next_states.take(index, axis=0, mode='wrap')
            reward[i]        = self.rewards.take(index, axis=0, mode='wrap')
            terminal[i]      = self.terminal.take(index, axis=0, mode='wrap')
        return state, action, next_state, reward, terminal
    def reset(self):
        """
        Reset the pointers of the table, i.e. we don't remove the data but
        we allow it to be overwritten from first index of the table
        """
        self.top = 0
        self.bottom = 0
        self.size = 0


class TransitionTableRankPrio:


    def __init__(self, conf):
        """

        """
        self.state_siz = conf.state_siz
        self.act_num = conf.act_num
        self.hist_len  = conf.n_channels
        self.batch_size = conf.minibatch_size
        self.max_transitions = conf.maxlen
        self.minibatch_size = conf.minibatch_size
        self.n_segments = self.minibatch_size

        self.prio_len = 0
        self.priolist = []
        self.size = 0
        self.top = 0
        self.bottom = 0

        # Option FLAG if we want to remove those items that we sampled
        # Default: TRUE
        self.remove_sampled = True
        self.removed_indices = []

        # memory for state transitions
        max_size = self.max_transitions
        self.states  = np.zeros((max_size, self.state_siz*self.hist_len))
        self.actions = np.zeros((max_size, self.act_num))
        self.next_states = np.zeros((max_size, self.state_siz*self.hist_len))
        self.rewards = np.zeros((max_size, 1))
        self.terminal = np.zeros((max_size, 1))

        conf_buffer = copy(conf)
        conf_buffer.maxlen = self.minibatch_size
        self.batch_buffer = TransitionTable(conf_buffer)

        self.uniform_sampling = conf.uniform_priority_sampling
        self.p_geometric = conf.p_geometric_priosampling



    def one_hot_action(self, actions):
        actions = np.atleast_2d(actions)
        one_hot_actions = np.zeros((actions.shape[0], self.act_num))
        for i in range(len(actions)):
            one_hot_actions[i, int(actions[i])] = 1
        return one_hot_actions

    def add(self, state, action, next_state, reward, terminal, q_loss, tf_variables, session):
        """
        Add new samples to buffer until it is full and feed it to the real
        transitiontable afterwards

        We need the current tensorflow session to calculate the q_loss

        """

        if self.batch_buffer.size == self.batch_buffer.max_transitions:

            state_batch, action_batch, next_state_batch, reward_batch, terminal_batch = self.batch_buffer.get_minibatch()


            self.batch_buffer.reset()
            feed_dict_train = {tf_variables[0] : state_batch,
                                   tf_variables[1] : action_batch,
                                   tf_variables[2] : next_state_batch,
                                   tf_variables[3] : reward_batch,
                                   tf_variables[4] : terminal_batch,
                                   K.learning_phase(): 0}
            train_err = session.run(q_loss, feed_dict = feed_dict_train)

            for ii in range(self.batch_buffer.max_transitions):
                self._add_internal(state_batch[ii],
                                  action_batch[ii],
                                  next_state_batch[ii],
                                  reward_batch[ii],
                                  terminal_batch[ii],
                                  train_err[ii])




        else:
            self.batch_buffer.add(state,action,next_state,reward,terminal)




    def _add_internal(self, state, action, next_state, reward, terminal,Q_loss):
        """
        add sample to the TransitionTable by following these steps:

        1. Check if there free insert indices due to previously removed data
            if yes: take the first index in that list as new index list
            if no: 1a. if table full:
                    1b) pop minimum q_loss and data_index (new insert index) from heap
               else:
                   1b) insert index is at the end of list

        2. Add data to insert index in transition table
        3. Add new q_loss and insert index to heap

        our priority is - Q_loss (minus Q_loss) since we want to pop the smallest
        and have the largest at the beginning
        """
        ## get insert index
        ## 1 check if there are previously removed indices
        if len(self.removed_indices) > 0:
            insert_index = self.removed_indices.pop()
        else:
            if self.size == self.max_transitions: #table full
                ##1b)
                q_loss_min, insert_index = self.priolist.pop()
                self.bottom = (self.bottom + 1) % self.max_transitions
            else:
                ## 1a)
                insert_index = self.top
                self.size += 1
                self.top = (self.top + 1) % self.max_transitions

        ## 2

        self.states[insert_index] = state
        self.actions[insert_index] = action
        self.next_states[insert_index] = next_state
        self.rewards[insert_index] = reward
        self.terminal[insert_index] = terminal

        ## 3

        #TO-DO; Can there be exact duplicates? I dont think so
        priority = -Q_loss
        heapq.heappush(self.priolist,(priority,insert_index))

        assert self.size -len(self.removed_indices) == len(self.priolist)

    def sample_minibatch(self, batch_size=None):
        """
        Sample a prioritized minibatch with the following steps

        1. Initialize batch

        2. In for loop:
            2a) sample random_index
            2b) get (q_loss,insert_index) from priolist array[random_index]
            2c) remove index random_index in priolist heap
            2d) add insert_index to the list of removed indices (to be overwritten later)
        3.  get data from insert_index

        """



        if batch_size is None:
            batch_size = self.batch_size
        state      = np.zeros((batch_size, self.state_siz*self.hist_len), dtype=np.float32)
        action     = np.zeros((batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((batch_size, self.state_siz*self.hist_len), dtype=np.float32)
        reward     = np.zeros((batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((batch_size, 1), dtype=np.float32)


        ## 2.
        for ii in range(batch_size):

            #2a)
            if self.uniform_sampling:
                random_index = randrange(len(self.priolist))
            else:

                random_index = np.random.geometric(self.p_geometric)
            #2b) -> largest q-loss at the END
            q_loss, insert_index = self.priolist[random_index]
            #2c)
            self.remove_at_index(random_index)
            #2d)
            self.removed_indices.append(insert_index)
            #3)
            state[ii]         = self.states.take(insert_index, axis=0, mode='wrap')
            action[ii]        = self.actions.take(insert_index, axis=0, mode='wrap')
            next_state[ii]    = self.next_states.take(insert_index, axis=0, mode='wrap')
            reward[ii]        = self.rewards.take(insert_index, axis=0, mode='wrap')
            terminal[ii]      = self.terminal.take(insert_index, axis=0, mode='wrap')
        #print(len(self.priolist))
        return state,action,next_state,reward,terminal



    def remove_at_index(self,i):
        """
        This is taken from:
        http://stackoverflow.com/questions/10162679/python-delete-element-from-heap

        We just hope this works <3
        """
        self.priolist[i] = self.priolist[-1] # overwrite i-th element with last element
        self.priolist.pop() #delete the last element (therefore also the one at i)

        # in case we have randomly selected the last item, don't heapify
        if i == len(self.priolist):
            return

        heapq._siftup(self.priolist, i) #heaoify up
        heapq._siftdown(self.priolist, 0, i)# heapify down
