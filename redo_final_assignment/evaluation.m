
addpath('../../Downloads/export_fig')
set(gcf,'Renderer','OpenGL')

% options
fnames = dir('log/*.log'); % the log directory, relative to current position
disp_on = true; % toggle plotting
print_on  = true;

show_perc_targets_reached = true;
show_q_loss = true;
show_avg_steps = true;
show_n_targets_reached = false;


for fnum = 1:length(fnames)
    fname = fnames(fnum).name;
    if ~isempty(strfind(fname, '_results'))
        continue;
    end
    disp(['----------------' fname '-----------------------']);
    % load csv file
    try
        data = csvread(['log' filesep fname]);
    catch
        continue;
    end

    step = data(:,1);
    % find begin of last run
    start = find(step == min(step), 1, 'last') + 1;
    step = data(start:end,1);
    q_err = data(start:end,2);
    epsilon = data(start:end,3);
    tgt_reach_ratio = data(start:end,4);
    tgt_reach_num = data(start:end,5);
    avg_steps = data(start:end,6);
    
    % calculate linear prediction
    Phi = [ones(size(step)) step];
    Phi_exp = [ones(size(step)) log(step)];
    theta_num = Phi \ tgt_reach_num;
    theta_ratio = Phi \ tgt_reach_ratio;
    theta_avg_steps = Phi \ avg_steps;
    theta_q_err = Phi \ log(q_err);
    
    % calculate variance
    Phi2_inv = inv(Phi.' * Phi);
    var_num = var(tgt_reach_num);
    var_num_est = var_num * Phi2_inv;
    var_ratio = var(tgt_reach_ratio);
    var_ratio_est = var_ratio * Phi2_inv;
    var_avg_steps = var(avg_steps);
    var_avg_steps_est = var_avg_steps * Phi2_inv;
    var_q_err = var(q_err);
    var_q_err_est = var_q_err * Phi2_inv;
    
    % calculate preditions
    step_100tgt = ceil((100 - theta_num(1)) / theta_num(2));
    phi_step_100tgt = [step_100tgt 1].';
    var_step_100tgt = phi_step_100tgt.' * var_num_est * phi_step_100tgt;
    std_step_100tgt = sqrt(var_step_100tgt);
    step_100per = ceil((100 - theta_ratio(1)) / theta_ratio(2));
    phi_step_100per = [step_100per 1].';
    var_step_100per = phi_step_100per.' * var_ratio_est * phi_step_100per;
    std_step_100per = sqrt(var_step_100per);
    
    % moving horizon estimator
    window_size = 51;
    moving_num = movingHorizonEstimator(tgt_reach_num, window_size);
    moving_ratio = movingHorizonEstimator(tgt_reach_ratio, window_size);
    moving_avg_steps = movingHorizonEstimator(avg_steps, window_size);
    moving_q_err = movingHorizonEstimator(q_err, window_size);
        
    % print some output
    fprintf('mean number of targets reached: %f\n', mean(tgt_reach_num));
    fprintf('variance of the above: %f\n', var(tgt_reach_num));
    if theta_num(2) > 0
        fprintf('trend rising, 100 targets reached at iteration %i\n', step_100tgt);
        fprintf('std of this estimate: %i ', std_step_100tgt);
        fprintf('earliest likely occasion: %i\n', step_100tgt - std_step_100tgt);
    else
        disp('trend falling');
    end
    
    fprintf('mean ratio of targets reached: %f\n', mean(tgt_reach_ratio));
    fprintf('variance of the above: %f\n', var_ratio);
    if theta_ratio(2) > 0   
        fprintf('trend rising, 100 percent of targets reached at iteration %i\n', step_100per);
        fprintf('std of this estimate: %i ', std_step_100per);
        fprintf('earliest likely occasion: %i\n', step_100per - std_step_100per);
    else
        disp('trend falling');
    end
    
    if (theta_num(2) > 0) && (theta_ratio(2) > 0)
        full_converge = max( step_100per , step_100tgt );
        fprintf('full convergence might be reached at %i\n', full_converge)
    else
        disp('might not converge');
    end
    
    fprintf('Average Q-loss: %f, standard deviation of Q-loss: %f', mean(q_err), std(q_err));
    
    fprintf('\n');
    
    if disp_on
        % plot some graphs
        %pos = [0 0 800 450];
        basename = strsplit(fname,'.');
        save_name = strcat('report',filesep,'figures',filesep,basename(1));
        %save_name  = strcat(filesep, 'tmp', filesep, basename(1));
        sp = 1;
        if show_perc_targets_reached
            if print_on
                h = clf(figure(sp));
                set(gca,'fontsize',font_size)
            else
                h = figure;
            end
            set(h, 'Position', pos);
            hold on; grid on;
            plot(step, tgt_reach_ratio);
            %plot(step, Phi* theta_ratio, 'r');
            plot(step, moving_ratio, 'r');
            xlabel('step');
            ylabel('Targets reached [%]');
            ylim([0,105]);
            if print_on
                print(h, char(strcat(save_name, '_ratio.eps')),'-depsc');
            end
            sp = sp+1;
        end
        if show_n_targets_reached
            if print_on
                h = clf(figure(sp));
                set(gca,'fontsize',font_size)
            else
                h = figure;
            end
            set(h, 'Position', pos);
            hold on; grid on;
            plot(step, tgt_reach_num);
            %plot(step, Phi*theta_num,'r');
            plot(step, moving_num, 'r');
            xlabel('step');
            ylabel('Number of targets reached');
            ylim([0,200]);
            if print_on
                print(h, char(strcat(save_name, '_num.eps')),'-depsc');
            end
            sp = sp +1;
        end
        if show_avg_steps
            if print_on
                h = clf(figure(sp));
                set(gca,'fontsize',font_size)
            else
                h = figure;
            end
            set(h, 'Position', pos);
            hold on; grid on;
            plot(step, avg_steps);
            %plot(step, Phi*theta_avg_steps, 'r');
            plot(step, moving_avg_steps, 'r');
            xlabel('step');
            ylabel('Average number of steps');
            ylim([0,100]);
            if print_on
                print(h, char(strcat(save_name, '_avg-steps.eps')),'-depsc');
            end
            sp = sp +1;
        end
        if show_q_loss
            if print_on
                h = clf(figure(sp));
                set(gca,'fontsize',font_size)
            else
                h = figure;
            end
            set(h, 'Position', pos);
            semilogy(step, q_err);
            hold on; grid on;
            semilogy(step, exp(Phi*theta_q_err), 'r')
            %plot(step, Phi*theta_q_err, 'r');
            %semilogy(step, moving_q_err, 'r');
            xlabel('step');
            ylabel('Q-Loss');
            %ylim([1e2,1e8]);
            if print_on
                print(h, char(strcat(save_name, '_Q-loss.eps')),'-depsc');
            end
            sp = sp +1;
        end
    end
end
