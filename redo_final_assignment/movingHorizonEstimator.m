function [ output_array ] = movingHorizonEstimator( input_array , window_length)
%MOVINGHORIZONESTIMATOR Summary of this function goes here
%   Detailed explanation goes here

%assert( ~isvector(input_array));
N  = length(input_array);

if window_length > N
    window_length = N;
end
steps_ahead = floor(window_length/2);
current_index = steps_ahead;
window = nan(window_length, 1);
window(1:steps_ahead) = input_array(1:steps_ahead);
output_array = zeros(size(input_array));

for i=1:N
    current_index = mod(current_index, window_length) + 1;
    input_idx = i+steps_ahead;
    if input_idx <= N
        window(current_index) = input_array(input_idx);
    else
        window(current_index) = nan;
    end
    output_array(i) = mean(window, 'omitnan');
end

