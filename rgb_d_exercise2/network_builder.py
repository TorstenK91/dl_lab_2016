#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 13:18:52 2016

@author: schoelst
"""
import tensorflow as tf
import numpy as np


def create_network(config, img_channels=1):
    """
    Parse a config file containing a model definition and translate this into
    a tensorflow graph.
    """

    # Some helper functions
    def weight_variable(shape, stddev):
        initial = tf.truncated_normal(shape, stddev=stddev)
        return tf.Variable(initial)

    def bias_variable(shape, bias):
        initial = tf.constant(bias, shape=shape)
        return tf.Variable(initial)

    last_layer = None
    last_output = None
    default_stddev  = 0.1
    default_bias = 0.1

    names = np.sort(config.keys())
    for name in names:
        layer = config[name]
        # layer definitions
        def conv(last_output):
            # convolution layer
            num_inputs = last_layer['num_units']
            num_units = layer['num_units']

            if 'kernel_size' in layer:
                kernel_size = layer['kernel_size']
            else:
                kernel_size = [3, 3]

            if 'stddev' in layer:
                stddev = layer['stddev']
            else:
                stddev = default_stddev

            if 'strides' in layer:
                strides = [1] + layer['strides'] + [1]
            else:
                strides = [1, 1, 1, 1]

            if 'padding' in layer:
                padding = layer['padding']
            else:
                padding = 'SAME'

            if 'bias' in layer:
                bias = layer['bias']
            else:
                bias = default_bias
            #print(strides)

            W = weight_variable(kernel_size + [num_inputs, num_units], stddev)
            b = bias_variable([num_units], bias)
            preactive = tf.nn.conv2d(last_output, W, strides=strides,
                                      padding=padding) + b
            return tf.nn.relu(preactive)

        def fully_connected(last_output):
            num_inputs = last_layer['num_units']
            num_units = layer['num_units']

            if 'stddev' in layer:
                stddev = layer['stddev']
            else:
                stddev = default_stddev

            if 'bias' in layer:
                bias = layer['bias']
            else:
                bias = default_bias

            if 'img_shape' in layer:
                img_shape = layer['img_shape']
                img_pixels = img_shape[0] * img_shape[1]
                num_inputs = img_pixels * num_inputs
            else:
                pass

            W = weight_variable([num_inputs,num_units], stddev)
            b = bias_variable([num_units], bias)

            lo_reshaped = tf.reshape(last_output, [-1, num_inputs])
            activation = tf.matmul(lo_reshaped, W) + b
            return tf.nn.relu(activation)

        def max_pool(last_output):
            if 'ksize' in layer:
                ksize = [1] + layer['ksize'] + [1]
            else:
                ksize = [1, 2, 2, 1]

            if 'strides' in layer:
                strides = [1] + layer['strides'] + [1]
            else:
                strides = [1, 2, 2, 1]

            if 'padding' in layer:
                padding = layer['padding']
            else:
                padding = 'SAME'
            layer['num_units'] = last_layer['num_units']

            return tf.nn.max_pool(last_output, ksize=ksize, strides=strides,
                           padding=padding)

        def update_defaults():
            if layer[0] == 'default_stddev':
                default_stddev = layer[1]
            elif layer[0] == 'default_bias':
                default_bias = layer[1]
            else:
                return False
            return True

        # create a new layer
        if not isinstance(layer, dict):
            # parse general settings
            if update_defaults():
                pass
            else:
                print("found gerneral seting: {}".format(layer))

        else:
            # parse layers
            ltype = layer['type'].lower()
            print("processing: {} of type: {}".format(name,ltype))
            if ltype == 'input':
                x = tf.placeholder(tf.float32,
                    shape=[None] + layer['img_shape'] + [img_channels])
                last_output = x
                layer['num_units'] = img_channels
            elif ltype == 'conv':
                last_output = conv(last_output)
            elif ltype == 'fully_connected':
                last_output = fully_connected(last_output)
            elif ltype == 'max_pool':
                last_output = max_pool(last_output)
            elif ltype == 'dropout':
                keep_prob = tf.placeholder(tf.float32)
                last_output = tf.nn.dropout(last_output, keep_prob)
                layer['num_units'] = last_layer['num_units']
            else:
                raise ValueError("The specified layer type is unkown: {}"
                                 .format(ltype))

            last_layer = layer

    return x,last_output


