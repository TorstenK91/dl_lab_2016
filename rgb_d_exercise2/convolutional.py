# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Simple, end-to-end, LeNet-5-like convolutional rgbd_10 model example."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import gzip
import os
import sys
import time

import numpy
import numpy as np
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

import optimizer_builder

from network_builder import create_network
from sklearn.metrics import confusion_matrix

import input_data

from configobj import ConfigObj
from validate import Validator

# TODO
# These are some useful constants that you can use in your code.
# Feel free to ignore them or change them.
# TODO
IMAGE_SIZE = 32
NUM_LABELS = 10
SEED = 66478  # Set to None for random seed.
BATCH_SIZE = 64
NUM_EPOCHS = 10
EVAL_BATCH_SIZE = 512
EVAL_FREQUENCY = 100  # Number of steps between evaluations.
# This is where the data gets stored
#TRAIN_DIR = 'data'
# HINT:
# if you are working on the computers in the pool and do not want
# to download all the data you can use the pre-loaded data like this:

#TRAIN_DIR = '/home/mllect/data/rgbd'
TRAIN_DIR = '/tmp/dl_data'
MODEL_DIR = 'models'

def data_type():
  """Return the type of the activations, weights, and placeholder variables."""
  return tf.float32

def error_rate(predictions, labels):
  """Return the error rate based on dense predictions and sparse labels."""
  return 100.0 - (
      100.0 *
      numpy.sum(numpy.argmax(predictions, 1) == labels) /
      predictions.shape[0])

def fake_data(num_images, channels):
  """Generate a fake dataset that matches the dimensions of rgbd_10 dataset."""
  data = numpy.ndarray(
      shape=(num_images, IMAGE_SIZE, IMAGE_SIZE, channels),
      dtype=numpy.float32)
  labels = numpy.zeros(shape=(num_images,), dtype=numpy.int64)
  for image in xrange(num_images):
    label = image % 2
    data[image, :, :, 0] = label - 0.5
    labels[image] = label
  return data, labels

def main(argv=None):  # pylint: disable=unused-argument

  if FLAGS.self_test:
    print('Running self-test.')
    NUM_CHANNELS = 1
    train_data, train_labels = fake_data(256, NUM_CHANNELS)
    validation_data, validation_labels = fake_data(EVAL_BATCH_SIZE, NUM_CHANNELS)
    test_data, test_labels = fake_data(EVAL_BATCH_SIZE, NUM_CHANNELS)
    num_epochs = 1
  else:
    if (FLAGS.use_rgbd):
      NUM_CHANNELS = 4
      print('****** RGBD_10 dataset ******')
      print('* Input: RGB-D              *')
      print('* Channels: 4               *')
      print('*****************************')
    else:
      NUM_CHANNELS = 3
      print('****** RGBD_10 dataset ******')
      print('* Input: RGB                *')
      print('* Channels: 3               *')
      print('*****************************')
    # Load input data
    data_sets = input_data.read_data_sets(TRAIN_DIR, FLAGS.use_rgbd)

    train_data = data_sets.train.images
    train_labels= data_sets.train.labels
    test_data = data_sets.test.images
    test_labels = data_sets.test.labels
    validation_data = data_sets.validation.images
    validation_labels = data_sets.validation.labels

  # TODO:
  # After this you should define your network and train it.
  # Below you find some starting hints. For more info have
  # a look at online tutorials for tensorflow:
  # https://www.tensorflow.org/versions/r0.11/tutorials/index.html
  # Your goal for the exercise will be to train the best network you can
  # please describe why you did chose the network architecture etc. in
  # the one page report, and include some graph / table showing the performance
  # of different network architectures you tried.
  #
  # Your end result should be for RGB-D classification, however, you can
  # also load the dataset with NUM_CHANNELS=3 to only get an RGB version.
  # A good additional experiment to run would be to cmompare how much
  # you can gain by adding the depth channel (how much better the classifier can get)
  # TODO:

  # This is where training samples and labels are fed to the graph.
  # These placeholder nodes will be fed a batch of training data at each
  # training step using the {feed_dict} argument to the Run() call below.

  test_model_after_training = False
  ini_file_path = FLAGS.config_path
  model_path = FLAGS.model_path
  if model_path:
      model_path = os.path.abspath(model_path)
      if not os.path.exists(model_path):
          print("WARNING: The specified model {} does not exist.".format(model_path))
          print("WARNING: Creating new weights instead!")
          model_path = None

  optimizer_settings, training_settings, architecture_settings = train_parser(ini_file_path)

  # read values from configuration
  num_epochs = training_settings["num_epochs"]
  batch_size = training_settings["batch_size"]
  image_size = training_settings["image_size"]
  num_labels = training_settings["num_labels"]
  eval_batch_size = training_settings["eval_batch_size"]
  eval_frequency = training_settings["eval_frequency"]

  train_labels_node = tf.placeholder(tf.int64, shape=(batch_size,))

  # TS-TODO: This should be specified in the architecture and appropriately dealt with during prediction/training
  keep_prob = tf.placeholder(tf.float32)

  # create network from configuration
  x,y_conv = create_network(architecture_settings, img_channels=NUM_CHANNELS)


  # TODO
  # compute the loss of the model here
  ## TK-TODO: y_conv should be output of the network
  ## TK-TODO: y_ should be training labels
  # TODO
  loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_conv, tf.one_hot(train_labels_node,num_labels)))

  # TODO
  # then create an optimizer to train the model
  # HINT: you can use the various optimizers implemented in TensorFlow.
  #       For example, google for: tf.train.AdamOptimizer()
  # TODO

  # TODO
  # Make sure you also define a function for evaluating on the validation
  # set so that you can track performance over time

  #TK-TODO: should be placeholder vor the y_labels
  y_labels = tf.placeholder(tf.int64,shape=(None,))
  #TK-TODO: we could (maybe should) make two distinct validation and test accuracy functions

  y_prediction = tf.argmax(y_conv,1)
  correct_prediction = tf.equal(tf.argmax(y_conv,1), y_labels)
  accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


  #create the optimizer and the parametrs which (may) change over the course of episodes
  optimizer,opti_params,opti_values = optimizer_builder.create_optimizer(optimizer_settings,num_epochs)


  train_step = optimizer.minimize(loss)
  # Create a local session to run the training.
  with tf.Session() as sess:
      # promt for restore
      if model_path:
          saver = tf.train.Saver()
          saver.restore(sess, model_path)
	  print("Model {} restored.".format(model_path))
      else:
          sess.run(tf.initialize_all_variables())

      #TK-TODO: could be a settings parameter
      shuffle = False

      # If we dont shuffle the input data,we can reuse the same iteartor for all epochs
      validation_batch_iterator = create_batch_iterator(validation_data,validation_labels,eval_batch_size)

      for epoch in range(num_epochs):

          #get the batch iterator if we want to shuffle

          batch_iterator = create_batch_iterator(train_data,train_labels,batch_size,shuffle = shuffle)

          #update parameters of the optimizer
          opti_feed_dict = optimizer_builder.create_feed_dict(opti_params,opti_values,epoch)

          ii = 0
          #print(train_data)
          for x_batch,y_batch in batch_iterator:

              if ii % eval_frequency == 0:
                  correct_predictions_all = list()
                  validation_batch_iterator = create_batch_iterator(validation_data,validation_labels,eval_batch_size)

                  for x_batch_val,y_batch_val in validation_batch_iterator:
                      #print(np.shape(x_batch_val))
                      #print(np.shape(y_batch_val))
                      #print(y_batch_val)
                      correct_predictions_all.append(correct_prediction.eval(feed_dict={
                        x: x_batch_val, y_labels: y_batch_val, keep_prob: 1.0}))
                      #print(correct_predictions_all)


                  validation_error = 1.0 - numpy.mean(numpy.asarray(correct_predictions_all,dtype='float'))

                  correct_predictions_train = correct_prediction.eval(feed_dict={
                        x: x_batch, y_labels: y_batch, keep_prob: 1.0})
                  train_error = 1.0 - numpy.mean( numpy.asarray(correct_predictions_train,dtype='float'))

                  print("epoch {}, batch number {}".format(epoch, ii))
                  print("  train error     : {}".format(train_error))
                  print("  validation error: {}".format(validation_error))


              ## TK-TODO: need to change x to the name of the input variable to the network
              ## TK-TODO: need to chance y to the name of the variable for evaluation
              ## TK-TODO:
              feed_dict={x: x_batch, train_labels_node : y_batch, keep_prob: 0.5}

              feed_dict.update(opti_feed_dict)


              train_step.run(feed_dict=feed_dict)

              ii += 1

      if test_model_after_training:
          # if we want the confusion matrix we evaluate the test output and calculate
          # the error "by hand"
          if confusion_matrix:


               test_batch_iterator = create_batch_iterator(test_data,test_labels,EVAL_BATCH_SIZE)
               ii = 0
               for x_batch_test,y_batch_test in test_batch_iterator:
                     y_test_pred_ii = y_prediction.eval(feed_dict={
                        x: x_batch_test, y_labels: y_batch_test, keep_prob: 1.0})

                     if ii == 0:
                           y_test_pred = y_test_pred_ii
                     else:
                           y_test_pred = np.vstack((y_test_pred,y_test_pred_ii))

                      #print(np.shape(x_batch_val))
                      #print(np.shape(y_batch_val))
                      #print(y_batch_val)

                      #print(correct_predictions_all)


               print(numpy.equal(numpy.argmax(y_test_pred),test_labels).astype('float'))
               test_error = numpy.mean(numpy.equal(numpy.argmax(y_test_pred),test_labels).astype('float'))
               print(confusion_matrix(test_labels,y_test_pred))

          # otherwise the theano function does the job for us
          else:
               correct_predictions_all = list()
               test_batch_iterator = create_batch_iterator(test_data,test_labels,EVAL_BATCH_SIZE)

               for x_batch_test,y_batch_test in test_batch_iterator:
                     correct_predictions_all.append(correct_prediction.eval(feed_dict={
                  x: x_batch_test, y_labels: y_batch_test, keep_prob: 1.0}))
                #print(np.shape(x_batch_val))
                #print(np.shape(y_batch_val))
                #print(y_batch_val)

                #print(correct_predictions_all)

               test_error = numpy.mean( numpy.asarray(correct_predictions_all,dtype='float'))

          print("Test error: {}".format(test_error))

      # promt for saving
      save = raw_input("Do you want to save the current weights? [Y/n] ")
      if save and save.lower()[0] == "n":
          save = False
      else:
          save = True

      if save:
          filename = ""
          while not filename:
              filename = raw_input("Please enter a filename: ")
          print("saving weights now...")
          saver = tf.train.Saver()
          filepath = os.path.join(MODEL_DIR, filename)
          filepath = os.path.abspath(filepath)
          saver.save(sess, filepath)
          print("Thanks! Your file is stored at {}".format(filepath))

    # TODO
    # Make sure you initialize all variables before starting the tensorflow training
    # TODO

    # Loop through training steps here
    # HINT: always use small batches for training (as in SGD in your last exercise)
    # WARNING: The dataset does contain quite a few images if you want to test something quickly
    #          It might be useful to only train on a random subset!
    # For example use something like :
    # for step in max_steps:
    # Hint: make sure to evaluate your model every once in a while
    # For example like so:
    #print('Minibatch loss: {}'.format(loss))
    #print('Validation error: {}'.format(validation_error_you_computed)

    # Finally, after the training! calculate the test result!
    # WARNING: You should never use the test result to optimize
    # your hyperparameters/network architecture, only look at the validation error to avoid
    # overfitting. Only calculate the test error in the very end for your best model!
    # if test_this_model_after_training:
    #     print('Test error: {}'.format(test_error))
    #     print('Confusion matrix:')
    #     # NOTE: the following will require scikit-learn
    #     print(confusion_matrix(test_labels, numpy.argmax(eval_in_batches(test_data, sess), 1)))




def create_batch_iterator(X,Y,batch_size,shuffle = False):

    if shuffle:
        raise NotImplementedError("Shuffling of train set not implemented")

    n_data = numpy.shape(X)[0]
    n_batches = n_data // batch_size

    batch_list = [None]*n_batches

    for b in range(n_batches):
        idx = range(b*batch_size, (b+1)*batch_size)
        batch_list[b] = (X[idx,:,:,:],Y[idx])

    return iter(batch_list)


def train_parser(conf_path):
    """
    Parse a config file containing the hyperparameters (design) of the network and the
    optimizer to be used
        Input:
            conf_path: Path to the ini file

        Output:
            optimizer_settings: the necessary settings for the optimizer
            training_settings:  training hyperparameters
            architecture_settings:  describition of the architecture

    """

    settings = ConfigObj(conf_path,configspec="config_specs.conf",stringify = True)

    settings.validate(Validator())
    optimizer_settings = settings["optimizer"].dict()
    training_settings = parse_training_settings(settings["training"].dict())
    architecture_settings = settings["architecture"].dict()

    return optimizer_settings,training_settings,architecture_settings





def parse_training_settings(training_settings):
    """
    Parse Training settings and set default values if not specified

    """

    if not "batch_size" in training_settings:
        training_settings["batch_size"] = BATCH_SIZE

    if not "num_epochs" in training_settings:
        training_settings["num_epochs"] = NUM_EPOCHS

    if not "image_size" in training_settings:
        training_settings["image_size"] = IMAGE_SIZE

    if not "num_labels" in training_settings:
        training_settings["num_labels"] = NUM_LABELS

    if not "eval_batch_size" in training_settings:
        training_settings["eval_batch_size"] = EVAL_BATCH_SIZE

    if not "eval_frequency" in training_settings:
        training_settings["eval_frequency"] = EVAL_FREQUENCY

    return training_settings



if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('config_path',help='The path to the ini file')
  parser.add_argument('--model_path',help='Path to pretrained model file', default=None)
  parser.add_argument(
      '--use_rgbd',
      default=False,
      help='Use rgb-d input data (4 channels).',
      action='store_true'
  )
  parser.add_argument(
      '--self_test',
      default=False,
      action='store_true',
      help='True if running a self test.'
  )
  FLAGS = parser.parse_args()

  tf.app.run()
