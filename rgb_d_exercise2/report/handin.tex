\documentclass[twoside,twocolumn]{article}

\usepackage{blindtext} % Package to generate dummy text throughout this template 

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[english]{babel} % Language hyphenation and typographical rules

\usepackage[hmarginratio=1:1,top=20mm,left=15mm,right=15mm,bottom=15mm,columnsep=20pt]{geometry} % Document margins
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables

\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text
%\usepackage{natbib}
\usepackage{enumitem} % Customized lists
\setlist[itemize]{noitemsep} % Make itemize lists more compact

\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\roman{subsection}} % roman numerals for subsections
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles

\usepackage{fancyhdr} % Headers and footers
\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{Deep Learning Lab $\bullet$ Nov 2016 $\bullet$ Tobias Sch\"ols, Torsten Koller} % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{titling} % Customizing the title section

\usepackage{hyperref} % For hyperlinks in the PDF

% additional packages
\usepackage{amsmath,amssymb}
%\usepackage{empheq}
\usepackage{graphics}

% custom definitions
\makeatletter
\newcommand{\pushright}[1]{\ifmeasuring@#1\else\omit\hfill$\displaystyle#1$\fi\ignorespaces}
\newcommand{\pushleft}[1]{\ifmeasuring@#1\else\omit$\displaystyle#1$\hfill\fi\ignorespaces}
\makeatother

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
\posttitle{\end{center}} % Article title closing formatting
\title{Deep Learning Lab: Exercise 2} % Article title
\author{Tobias Sch\"ols, Torsten Koller%
%\textsc{John Smith}\thanks{A thank you or further information} \\[1ex] % Your name
%\normalsize University of California \\ % Your institution
%\normalsize \href{mailto:john@smith.com}{john@smith.com} % Your email address
%\and % Uncomment if 2 authors are required, duplicate these 4 lines if more
%\textsc{Jane Smith}\thanks{Corresponding author} \\[1ex] % Second author's name
%\normalsize University of Utah \\ % Second author's institution
%\normalsize \href{mailto:jane@smith.com}{jane@smith.com} % Second author's email address
}
\date{\today} % Leave empty to omit a date


%-----------------------------------------------------------------
\begin{document}
\maketitle

\begin{abstract}
	In this report, we present the results of training a convolutional neural network (CNN) from scratch to classify 10 different objects on a downsampled version of the RGB-D dataset \cite{lai2011large}. Our best trained 9-Layer CNN achieves a test error result of $0.0882$.
\end{abstract}

% Print the title


\section{Architecture and Hyperparameters}

To identify a well-performing network, a number of different hyperparameters had to be adjusted and the resulting networks had to be trained and evaluated on the validation set. We differentiate between two types of parameters: architecture related and optimization related parameters. In tables \ref{network_hyperparam} and \ref{training_hyperparam}, we show parameters for the network and the training respectively, which we experienced as to be most influential to the validation error.


\begin{table}[h!]
	\centering
	\begin{tabular}{l|c|c}
		Hyperparameter & range & scope \\
		\hline
		\hline
		order / type of layers & various & -  \\
		\hline
		\# layers & 4-10 & -  \\
		\hline
		\# hidden units & 32 - 1024 & conv2d, fc \\
		\hline
		 
		bias init (const) & $0.0 - 1.0$ & conv2d,fc \\
		\hline
		weight init $\sim N(0,\sigma)$ & 0.01 - 0.2 & conv2d, fc\\
		\hline
		kernel size & [2,2] , [3,3], [5,5] & conv2d \\
		 
	\end{tabular}
	
	\caption{Most influential network related hyperparameters.}
	\label{network_hyperparam}
\end{table}

\begin{table}[h!]
	\centering
	\begin{tabular}{l|c|c}
		Hyperparameter & range & scope \\
		\hline
		\hline
		optimizer & Adam, SGD, Momentum & -  \\
		\hline
		learning rate & $ 10^{-3}- 10^{-7}$ & -  \\
		\hline
		epsilon & $ 10^{-8} - 1.0 $& Adam \\
		\hline
		learning rate decay & various & - \\
		\hline 
	\end{tabular}
	
	\label{training_hyperparam}
	\caption{Most influential training and optimization related hyperparameters.}
\end{table}

A exhaustive list of parameters, that were adjusted to find a good network architecture can be found in the code repository \footnote{\url{https://bitbucket.org/TorstenK91/dl_lab_2016}; with the example configuration file example\_config.conf containing all possible parameters} 

\section{Evaluation}

Evaluation of the various architecture settings was performed by training on $\sim27700$ training samples and evaluated on $\sim9000$ validation images, to adjust the hyperparameters. To limit the vast number of possible architectures to be chosen from, the types of layers were limited to convolutional (conv2d), max-pooling and fully connected (fc) layers. The type of nonlinearity was fixed to rectified linear units (ReLus), which are typically used in state-of-the-art CNNs. \newline
During the adjustment and evaluation phase, a number of observations were made:
\begin{itemize}
	\item Compared to the training of multilayer perceptrons (mlps), the CNN training showed to be extremely sensitive to training hyperparameters, such as learning rate and initialization.	
	\item Configurations had to be evaluated various times, due to the extreme variance in validation error, introduced by the randomness of the initialization and the stochastic optimization procedure
	\item Introducing more than two or three convolutional layers and more than one max-pooling layer significantly aggravated training to the point that no improvements could be observed from these additional layers. 
\end{itemize} 




%%
%% 
%%The results in table 1 suggest two readings: First there seems to be a sweet spot for the network size. While the networks using two layers with 100 units each reach about $5\%$ error larger networks achieve better results. However, if we have a look at the last 2 results we see, that a smaller network achieved an almost equal result to the last and largest one. Second we see that the performance of a particular network also depends on the learning rate decay method. For smaller networks exponential decay shows better results, however with increasing network size the step decay shows better performance. This might be an effect of the chosen parameters ($k$-factor and step parameters), sadly a full investigation on this would have exceeded the frame of this exercise.\\
%%For further analysis the author choose a network using two fully connected layers the first with 256 units and a second full connected layer with 512 units, both layers use ReLU activations. This network performed well with the step decay method and seems to be a good compromise between size and performance. The results show (figure 1-6) the progress a training session including 30 epochs and a slightly reduced standard deviation of $0.05$ was used. It reached a test error of $0.0236$.
%%%
%%%\begin{figure}
%	\centering
%	\includegraphics[width=0.9\linewidth]{images/error}
%	\caption{Progression of classification error}
%	\label{fig:error}
%\end{figure}
%
%\begin{figure}[]
%	\centering
%	\includegraphics[width=0.9\linewidth]{images/loss}
%	\caption{Progression of loss}
%	\label{fig:loss}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\includegraphics[width=0.9\linewidth]{images/correctly_identified_number_[8]__6}
%	\caption{Correctly identified 4}
%	\label{fig:pos1}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\includegraphics[width=0.9\linewidth]{images/correctly_identified_number_[2]__7}
%	\caption{Correctly identified 9}
%	\label{fig:pos2}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\includegraphics[width=0.9\linewidth]{images/wrongly_identified_number_[4]_as[6]__7}
%	\caption{Mistook 4 for 6}
%	\label{fig:neg1}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\includegraphics[width=0.9\linewidth]{images/wrongly_identified_number_[9]_as[8]__4}
%	\caption{Mistook 9 for 8}
%	\label{fig:neg2}
%\end{figure}

\paragraph{Influence of Depth Information}
All network configurations, which showed reasonable performance on the 3-channel RGB dataset, were also trained on the 4-channel RGB-D dataset containing information about the depth of each pixel. While 
\section{Final Architecture and Training Procedure}
The final architecture \footnote{For a full description of the parameters, see \url{https://bitbucket.org/TorstenK91/dl_lab_2016/src/bfa341dd0c58aefc8bc08c47445f907d3a4534ba/rgb_d_exercise2/config/config7.ini?at=master&fileviewer=file-view-default}} used to compute the test-error, was trained using the Adam optimizer with a learning-rate fixed to $\tau = 10^{-5}$ on batches of size 64 over the period of 30 epochs. The architecture consists of the input-layer, followed by two convolutional layer with kernel size $[3, 3]$, strides of $[2,2]$ with 64 hidden units and max-pooling the output filter-bank, with a 2-by-2 pooling window. The resulting $[16,16,96]$ filter-map was again convolved twice with 128 hidden units using a $[3,3]$ kernel and stride of $[2,2]$. Before computing the softmax output (10 hidden units), two fully connected layers with 256 hidden units each were applied. \newline 
We report a final test-error of $0.0882$


\section{Summary \& Outlook}
A CNN was trained to classify objects in an RGB-D 
Based on the observations made during evaluation, deeper networks seem to be extremely sensitive towards initialization and may be tackled via more sophisticated initialization (e.g. Xavier initialization \cite{glorot2010understanding}).

\bibliography{handin}
\bibliographystyle{ieeetr}  
%\bibliographystyle{humannat}

\end{document}
