#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 09:18:52 2016

@author: kollert
"""

import numpy as np
from tensorflow import train
import tensorflow as tf


def create_optimizer(optimizer_settings,max_epochs):
    """
    Specify the necessary information to create an tesnroflow optimizer

    Input:
        optimizer_settings: Dictionariy which has to contain all necessary information to create an
                            optimizer object (at least: name + required parameters for
                            said optimizer). top level dictionary entries should be:
                                name: Name of the params
                                params: the params which dont change over the course of epochs
                                scheduled: the params which change over the course of epochs (see below)

                            scheduled:
                                if there is an option for the optimizer, which should change over the course of epochs
                                (e.g.: learning rate schedule, which changes the learning rate), it has to
                                be specified in a subdictionary with key "scheduled": The subdictionary has
                                to be declared s.t. each key is the name of the parameter and the value is
                                a list of size n_epochs containing the parameter value for each epoch
    Output:
        optimizer: The Optimizer object from tf.train



    """




    #get the parameters, which vary over the course of episodes
    _optimizer_settings_copy = optimizer_settings.copy()
    print(type(_optimizer_settings_copy))
    _optimizer_settings_init = dict()
    _optimizer_settings_rounds = dict()
#    print(_optimizer_settings)
    if "scheduled" in _optimizer_settings_copy:
        scheduled_params = _optimizer_settings_copy["scheduled"]


        for key,value in scheduled_params.iteritems():

            error_msg = """scheduled parameters need to be a list of size n_epochs containing the
                        parameter values for each epoch!"""


            if hasattr(value,"__len__"):



                if len(value) == max_epochs:
                    param_variable = tf.placeholder(tf.float32)
                    _optimizer_settings_rounds[param_variable] = value
                    _optimizer_settings_init[key] = param_variable
                else:
                    raise ValueError(error_msg)

            else:
                raise ValueError(error_msg)


        _optimizer_settings_copy.pop("scheduled")



    if "name" in _optimizer_settings_copy:
        optimizer_name = _optimizer_settings_copy["name"]
        _optimizer_settings_copy.pop("name")
    else:
        raise ValueError("Optimizer settings have to specify (at least) the name of the Optimizer")

    if "lr_scheduler" in _optimizer_settings_copy:
        lr_scheduler_settings = _optimizer_settings_copy["lr_scheduler"]
        param_variable = tf.placeholder(tf.float32)

        _optimizer_settings_init["learning_rate"] = param_variable
        _optimizer_settings_copy.pop("lr_scheduler")



        lr_list = [None]*max_epochs

        for ii in range(max_epochs):
              lr_list[ii] = learning_rate_from_scheduler(ii,lr_scheduler_settings)

        _optimizer_settings_rounds[param_variable] = lr_list
    print(_optimizer_settings_init)
    for key,value in _optimizer_settings_copy.iteritems():
          param_variable = tf.placeholder(tf.float32)
          _optimizer_settings_init[key] = param_variable
          _optimizer_settings_rounds[param_variable] = value

    if optimizer_name == "GradientDescentOptimizer":
        optimizer = train.GradientDescentOptimizer(**_optimizer_settings_init)

    elif optimizer_name == "AdadeltaOptimizer":
        optimizer =  train.AdadeltaOptimizer(**_optimizer_settings_init)

    elif optimizer_name == "MomentumOptimizer":
        optimizer = train.MomentumOptimizer(**_optimizer_settings_init)

    elif optimizer_name == "AdamOptimizer":
        optimizer = train.AdamOptimizer(**_optimizer_settings_init)

    elif optimizer_name == "FtrlOptimizer":
        optimizer =  train.FtrlOptimizer(**_optimizer_settings_init)

    elif optimizer_name == "RMSPropOptimizer":
        optimizer =  train.RMSPropOptimizer(**_optimizer_settings_init)


    else:
        error_msg = "Optimizer {} is not supported for training".format(optimizer_name)
        raise ValueError(error_msg)

    parameters = _optimizer_settings_rounds.keys()
    values = _optimizer_settings_rounds.values()

    return optimizer, parameters, values



def test_optimizer():

    test_settings = dict()
    test_settings["name"] = "GradientDescentOptimizer"
    test_settings["learning_rate"] = 0.001
    test_settings["use_locking"] = True


    print(create_optimizer(0,test_settings))



def learning_rate_from_scheduler(epoch,settings):

    sched_type = settings["type"]
    sched_params = settings["params"]
    if sched_type == 'exp_decay':
        learning_rate = float(sched_params["learning_rate"])
        decay_param = float(sched_params["decay_param"])
        return learning_rate * np.exp(-decay_param*epoch)

    elif sched_type == "step_decay":
        learning_rate = float(sched_params["learning_rate"])
        if epoch == 0:
            return learning_rate
        else:
            step_param_1 = float(sched_params["step_param_1"])
            step_param_2 = float(sched_params["step_param_2"])
            return learning_rate * (1. - step_param_1 * (epoch % step_param_2 == 0))

    elif sched_type == "1/t":
        learning_rate = float(sched_params["learning_rate"])
        inv_decay_param = float(sched_params["inv_decay_param"])
        return learning_rate / (1 +inv_decay_param* epoch)

    else:
        raise ValueError("unknown learning rate scheduler {}".format(sched_type))

def _exp_decay_lr(epoch):
    raise NotImplementedError("Bitte implementieren,Tobi")



def create_feed_dict(optimizer_variables,values,epoch):
      feed_dict = dict()
      for ii in range(len(optimizer_variables)):
            if hasattr(values[ii], '__iter__'):
                  print("lr_in epoch {}:{}".format(epoch,values[ii][epoch]))
                  print(values[ii][epoch])
                  feed_dict[optimizer_variables[ii]] = values[ii][epoch]
            else:
               feed_dict[optimizer_variables[ii]] = values[ii]
      return feed_dict




def parse_training_settings(epoch):
    """
    Parse Training settings and set default values if not specified

    """


    raise NotImplementedError("")



if __name__ == "__main__":
    test_optimizer()
