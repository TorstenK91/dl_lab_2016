import numpy as np

# custom modules
from utils     import Options, loadModel, restoreWeights, saveWeights, reshapeData
from simulator import Simulator
from transitionTable import TransitionTable


def main(argv=None):
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # NOTE:
    # this script assumes you did generate your data with the get_data.py script
    # you are of course allowed to change it and generate data here but if you
    # want this to work out of the box first run get_data.py
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # 0. initialization
    opt = Options()
    sim = Simulator(opt.map_ind, opt.cub_siz, opt.pob_siz, opt.act_num)
    trans = TransitionTable(opt.state_siz, opt.act_num, opt.hist_len,
                                 opt.minibatch_size, opt.valid_size,
                                 opt.states_fil, opt.labels_fil)

    # load model
    model, batch_size, nb_epoch = loadModel(FLAGS.model_path)

    if FLAGS.visualize_model:
          from IPython.display import SVG

          from keras.utils.visualize_util import model_to_dot
          SVG(model_to_dot(model)).create(prog='dot',format='svg')
          from keras.utils.visualize_util import plot
          #plot(model,to_file='model.png')
    # restore weights
    restoreWeights(model, FLAGS.weights)


    # 1. train
    ######################################
    # TODO implement your training here!
    # you can get the full data from the transition table like this:
    #
    # # both train_data and valid_data contain tupes of images and labels
    # train_data = trans.get_train()
    # valid_data = trans.get_valid()
    #
    # alternatively you can get one random mini batch line this
    #
    # for i in range(number_of_batches):
    #     x, y = trans.sample_minibatch()
    ######################################

    # load data
    X_train, Y_train = trans.get_train()
    X_val, Y_val = trans.get_valid()
    X_train = reshapeData(X_train, opt)
    X_val = reshapeData(X_val, opt)

    # train model

    hist = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch,
                     validation_data=(X_val, Y_val), verbose=2)
    # print(hist)
    # evaluate the model
    scores = model.evaluate(X_val, Y_val, verbose=0)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))


    # 2. save your trained model
    saveWeights(model)



if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('model_path',help='The path to the model definiton file')
    parser.add_argument(
        '--weights',
        help='Path to pretrained weights file',
        default=None
    )
    parser.add_argument(
        '--visualize_model',
        help='Set this FLAG to create an image of the model architecture. When Flag is set, no training is done! ',
        action = 'store_true',
        default=False
    )
    FLAGS = parser.parse_args()
    main()