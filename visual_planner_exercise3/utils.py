import numpy as np
from importlib import import_module
from os import mkdir, getcwd, chdir
from os.path import abspath, join, exists, dirname, basename
import tensorflow as tf
WEIGHTS_DIR = "pretrained"

class Options:
    #
    disp_on = False # you might want to set it to False for speed
    map_ind = 2
    change_tgt = True
    states_fil = "states.csv"
    labels_fil = "labels.csv"
    network_fil = "network.json"
    weights_fil = "network.h5"
    # simulator config
    disp_interval = .005
    if map_ind == 0:
        cub_siz = 5
        pob_siz = 5 # for partial observation
        # this defines the goal positionw
        tgt_y = 12
        tgt_x = 11
        early_stop = 200
    elif map_ind == 1:
        cub_siz = 5
        pob_siz = 5 # for partial observation
        # this defines the goal positionw
        tgt_y = 5
        tgt_x = 5
        early_stop = 75
    elif map_ind == 2:
        cub_siz = 5
        pob_siz = 5 # for partial observation
        # this defines the goal positionw
        tgt_y = 6
        tgt_x = 6
        early_stop = 75
    state_siz = (pob_siz * cub_siz) ** 2 # when use pob as input
    if change_tgt:
        tgt_y = None
        tgt_x = None
    act_num = 5

    # traing hyper params
    hist_len = 20
    minibatch_size  = 32
    n_minibatches   = 500
    valid_size      = 500
    eval_nepisodes  = 1000

    data_steps  = n_minibatches * minibatch_size + valid_size
    eval_steps  = early_stop * eval_nepisodes
    eval_freq   = n_minibatches # evaluate after each epoch
    prog_freq   = 500

class State: # return tuples made easy
    def __init__(self, action, reward, screen, terminal, pob):
        self.action   = action
        self.reward   = reward
        self.screen   = screen
        self.terminal = terminal
        self.pob      = pob


# The following functions were taken from scikit-image
# https://github.com/scikit-image/scikit-image/blob/master/skimage/color/colorconv.py

def rgb2gray(rgb):
    if rgb.ndim == 2:
        return np.ascontiguousarray(rgb)

    gray = 0.2125 * rgb[..., 0]
    gray[:] += 0.7154 * rgb[..., 1]
    gray[:] += 0.0721 * rgb[..., 2]

    return gray


def loadModel(model_path):
    """
    This function loads a model specified in model_path
    """
    model_path = abspath(model_path)
    if not exists(model_path):
        raise(ValueError("The specified model does not exist!"))
    else:
        filename = basename(model_path)
        module_name = filename.split('.')[0]
        module_name = 'models.' + module_name
        model_definition = import_module(module_name)
        return model_definition.createModel()


def restoreWeights(model, weights_path=None, by_name=True):
    """
    This function loads pretrained weights specified in weights_path
    """
    if weights_path:
        weights_path = abspath(weights_path)
        if not exists(weights_path):
            raise(ValueError("The specified weights file does not exist!"))
        else:
            model.load_weights(weights_path, by_name=by_name)
            print("Weights {} restored.".format(weights_path))
    else:
        pass

    return model


def saveWeights(model):
    """
    This function saves the weights of a session into file `weights_name`.
    """

    save = raw_input("Do you want to save the current weights? [Y/n] ")
    if save and save.lower()[0] == "n":
        pass
    else:
        filename = ""
        while not filename:
            filename = raw_input("Please enter a filename: ")
        print("saving weights now...")

        weights_dir = abspath(WEIGHTS_DIR)
        if not exists(weights_dir):
            mkdir(weights_dir)
            print("created {}".format(weights_dir))
        filepath = join(weights_dir, filename)

        model.save_weights(filepath)
        print("The current weights are safely stored at {}".format(filepath))

def reshapeData(data, opt):
    """ reshapes data from (num_datapoints, state_size) to
    (num_datapoints, hist_size, img_size, img_size)"""
    hist_size = opt.hist_len
    num_datapoints = data.shape[0]
    img_size = opt.pob_siz * opt.cub_siz
    assert(data.shape[1] == img_size**2 * hist_size)
    new_shape = [num_datapoints, hist_size, img_size, img_size]
    #print("reshaped data from: {} to: {}".format(data.shape, new_shape))
    data = data.reshape(new_shape)
    return np.rollaxis(data, 1, 4)

