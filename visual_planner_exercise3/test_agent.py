import numpy as np
import pylab as pl
from numpy.random import multinomial, uniform

# custom modules
from utils     import Options,restoreWeights,loadModel,rgb2gray,reshapeData
from simulator import Simulator
from transitionTable import TransitionTable

# 0. initialization


# TODO: load your agent

def run_agent(weights_path = None,model_path = None, sample_policy = False):
      """

      inputs:
            sample_policy: Allows for sampling from the learned policy according
                          to the pdf given via the softmax output of the CNN.


      """

      if model_path is None:
            model = None
      else:
            model = loadModel(model_path)[0]

            model = restoreWeights(model,weights_path = weights_path)


      # 0. initialization
      opt = Options()


      agent = Agent(opt.act_num,model = model,sample_policy = sample_policy)

      sim = Simulator(opt.map_ind, opt.cub_siz, opt.pob_siz, opt.act_num)

      # TODO: load your agent
      #agent =None
      trans_table = TransitionTable(opt.state_siz, opt.act_num, opt.hist_len,
                                 opt.minibatch_size, opt.valid_size,
                                 opt.states_fil, opt.labels_fil)

      # 1. control loop
      if opt.disp_on:
          win_all = None
          win_pob = None
      epi_step = 0    # #steps in current episode
      nepisteps = 0   # number of steps taken for all solutions
      nepisodes = 0   # total #episodes executed
      nepisodes_solved = 0
      action = 0     # action to take given by the network

      # start a new game
      state = sim.newGame(opt.tgt_y, opt.tgt_x)



      for step in range(opt.eval_steps):

          # check if episode ended
          if state.terminal or epi_step >= opt.early_stop:
              epi_step = 0
              nepisodes += 1
              if state.terminal:
                  nepisodes_solved += 1
              # start a new game
              state = sim.newGame(opt.tgt_y, opt.tgt_x)
          else:
              #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              # TODO: here you would let your agent take its action
              #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              # this just gets a random action

              #print(state.screen)

              pob_rgb = state.pob
              pob_grey = rgb2gray(pob_rgb)
              pop_grey_vectorize = np.ndarray.flatten(pob_grey)
              trans_table.add_recent(epi_step,pop_grey_vectorize)
              pob_hist = trans_table.get_recent()

              pop_hist_images = reshapeData(pob_hist, opt)


              action = agent.action(pop_hist_images)
              state = sim.step(action)

              epi_step += 1

          if state.terminal or epi_step >= opt.early_stop:
              if state.terminal:
                  nepisodes_solved += 1
                  nepisteps += epi_step
              epi_step = 0
              nepisodes += 1
              # start a new game
              state = sim.newGame(opt.tgt_y, opt.tgt_x)

          if step % opt.prog_freq == 0:
              print(step)

          if opt.disp_on:
              if win_all is None:
                  pl.figure()
                  win_all = pl.imshow(state.screen)
                  pl.figure()
                  win_pob = pl.imshow(state.pob)
              else:
                  win_all.set_data(state.screen)
                  win_pob.set_data(state.pob)
              pl.pause(opt.disp_interval)
              pl.draw()

      # 2. calculate statistics
      print("Fraction Episodes solved: {}".format(float(nepisodes_solved) / float(nepisodes)))
      print("Average number of steps: {}".format(float(nepisteps) / float(nepisodes_solved)))
      # 3. TODO perhaps  do some additional analysis

class Agent:

      def __init__(self,action_size,model = None, sample_policy = False):
            if model is None:
                  model = RandomActionModel(action_size)
            self.model = model
            self.sample_policy = sample_policy

      def action(self,state):

            if self.sample_policy:
                  return self._sample_action(self.model.predict(state)[0])
            else:
                  return np.argmax(self.model.predict(state))

      def _sample_action(self,softmax_output):
            """
            Sample an action from the softmax output of the CNN

            """
            x = uniform()
            if x <= 1.0:
                softmax_square = softmax_output#np.square(softmax_output)
                softmax_norm = softmax_square / (np.sum(softmax_square) + 1e-5)
                action = multinomial(1,softmax_norm)
            else:
                action = softmax_output

            return np.argmax(action)




class RandomActionModel:

      def __init__(self,action_size):

            self.act_size = action_size

      def predict(self,x):
            return randrange(self.act_size)




if __name__ == '__main__':
      #raise NotImplementedError()

      import argparse

      parser = argparse.ArgumentParser()

      parser.add_argument(
        '--weights',
        help='Path to pretrained weights file',
        default=None
          )
      parser.add_argument(
        '--model',
        help='Path to model file',
        default=None
          )

      parser.add_argument(
        '--sample_policy',
        help='Set to True if actions should be sampled from the learned policy.',
        default=False,
        action='store_true'
          )

      FLAGS = parser.parse_args()

      run_agent(weights_path = FLAGS.weights, model_path = FLAGS.model, sample_policy = FLAGS.sample_policy)
