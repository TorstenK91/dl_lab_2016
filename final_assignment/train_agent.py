import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from keras import backend as K
from copy import copy

# custom modules
from utils     import *
from simulator import Simulator
from transitionTable import TransitionTable,TransitionTableRankPrio


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NOTE:
# this is a little helper function that calculates the Q error for you
# so that you can easily use it in tensorflow as the loss
# you can copy this into your agent class or use it from here
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




def Q_loss_sum(Q_s, action_onehot, Q_s_next, best_action_next, reward, terminal, discount=0.99):
    """
    All inputs should be tensorflow variables!
    We use the following notation:
       N : minibatch size
       A : number of actions
    Required inputs:
       Q_s: a NxA matrix containing the Q values for each action in the sampled states.
            This should be the output of your neural network.
            We assume that the network implments a function from the state and outputs the
            Q value for each action, each output thus is Q(s,a) for one action
            (this is easier to implement than adding the action as an additional input to your network)
       action_onehot: a NxA matrix with the one_hot encoded action that was selected in the state
                      (e.g. each row contains only one 1)
       Q_s_next: a NxA matrix containing the Q values for the next states.
       best_action_next: a NxA matrix with the best current action for the next state
       reward: a Nx1 matrix containing the reward for the transition
       terminal: a Nx1 matrix indicating whether the next state was a terminal state
       discount: the discount factor
    """
    # calculate: reward + discount * Q(s', a*),
    # where a* = arg max_a Q(s', a) is the best action for s' (the next state)
    target_q = (1. - terminal) * discount * tf.reduce_sum(best_action_next * Q_s_next, 1, keep_dims=True) + reward
    # NOTE: we insert a stop_gradient() operation since we don't want to change Q_s_next, we only
    #       use it as the target for Q_s
    target_q = tf.stop_gradient(target_q)
    # calculate: Q(s, a) where a is simply the action taken to get from s to s'
    selected_q = tf.reduce_sum(action_onehot * Q_s, 1, keep_dims=True)
    loss = tf.reduce_sum(tf.square(selected_q - target_q))
    return loss


def Q_loss(Q_s, action_onehot, Q_s_next, best_action_next, reward, terminal, discount=0.99):
    """ Compute the sample wise Q_loss of a batch of (s,a,s',a',r) tuples

    All inputs should be tensorflow variables!
    We use the following notation:
       N : minibatch size
       A : number of actions
    Required inputs:
       Q_s: a NxA matrix containing the Q values for each action in the sampled states.
            This should be the output of your neural network.
            We assume that the network implments a function from the state and outputs the
            Q value for each action, each output thus is Q(s,a) for one action
            (this is easier to implement than adding the action as an additional input to your network)
       action_onehot: a NxA matrix with the one_hot encoded action that was selected in the state
                      (e.g. each row contains only one 1)
       Q_s_next: a NxA matrix containing the Q values for the next states.
       best_action_next: a NxA matrix with the best current action for the next state
       reward: a Nx1 matrix containing the reward for the transition
       terminal: a Nx1 matrix indicating whether the next state was a terminal state
       discount: the discount factor
    """
    # calculate: reward + discount * Q(s', a*),
    # where a* = arg max_a Q(s', a) is the best action for s' (the next state)
    target_q = (1. - terminal) * discount * tf.reduce_sum(best_action_next * Q_s_next, 1, keep_dims=True) + reward
    # NOTE: we insert a stop_gradient() operation since we don't want to change Q_s_next, we only
    #       use it as the target for Q_s
    target_q = tf.stop_gradient(target_q)
    # calculate: Q(s, a) where a is simply the action taken to get from s to s'
    selected_q = tf.reduce_sum(action_onehot * Q_s, 1, keep_dims=True)
    sample_wise_loss = tf.square(selected_q - target_q)
    return sample_wise_loss


def get_tf_variables(model,conf):
    """
    Create the tensorflow variables necessary for the training graph

    Required inputs:
        model: the Keras model of the Q-function
        conf: the config


    """
    n_channels = conf.hist_len
    if conf.use_target_view:
        n_channels += 1
    size_x = n_channels*conf.state_siz


    x = tf.placeholder(tf.float32, shape=(conf.minibatch_size, size_x))
    u = tf.placeholder(tf.float32, shape=(conf.minibatch_size, conf.act_num))
    xn = tf.placeholder(tf.float32, shape=(conf.minibatch_size, size_x))
    r = tf.placeholder(tf.float32, shape=(conf.minibatch_size, 1))
    term = tf.placeholder(tf.float32, shape=(conf.minibatch_size, 1))
    Q = model(tf_reshapeData(x, conf,conf.minibatch_size))


    if conf.use_target_network:
        tmodel, _, _ = loadConfig(FLAGS.config_path)
        Qn = tmodel(tf_reshapeData(xn, conf,conf.minibatch_size))
        copyWeights(model, tmodel)
        Qn.trainable = False
    else:
        Qn = model(tf_reshapeData(xn, conf, conf.minibatch_size))
        tmodel = None

    ustar = tf.one_hot(tf.arg_max(Qn,1),5)

    return x,u,xn,ustar,r,term,Q,Qn,tmodel



def get_feed_dict(var_list,batch,training_mode):
    """
    Create a feed dict from the list of necessary variables

    """

    if training_mode:
        keras_train = 1
    else:
        keras_train = 0

    feed_dict = {var_list[0] : batch[0],
                 var_list[1] : batch[1],
                 var_list[2] : batch[2],
                 var_list[3] : batch[3],
                 var_list[4] : batch[4],
                 K.learning_phase(): keras_train}
    return feed_dict

def main(arfv = None):



    # 0. initialization
    model, optimizer, conf = loadConfig(FLAGS.config_path)
    restoreWeights(model, FLAGS.weights)

    # define network inputs, outputs, loss, ...
    x,u,xn,ustar,r,term,Q,Qn,tmodel = get_tf_variables(model,conf)
    varlist = (x,u,xn,r,term,Q)
    q_loss_single = Q_loss(Q, u, Qn, ustar, r, term,discount = conf.discount)
    loss = Q_loss_sum(Q, u, Qn, ustar, r, term,discount = conf.discount)

    # setup simulations
    sim = Simulator(conf.map_ind, conf.cub_siz, conf.pob_siz, conf.act_num,
                    conf.tgt_pobsize)

    if conf.disp_on:
        win_all = None
        win_pob = None

    epi_step = 0
    nepisodes = 0

    # setup a large transitiontable that is filled during training
    # Training Data
    if conf.minibatch_prio_sampling:
        trans = TransitionTableRankPrio(conf)
        buff_conf = copy(conf)
        buff_conf.maxlen = buff_conf.minibatch_size
        batch_buff = TransitionTable(buff_conf)
    else:
        trans = TransitionTable(conf)
    # Validation Data
    trans_val = TransitionTable(conf)


    state = sim.newGame(conf.tgt_y, conf.tgt_x)

    target_dim = 0
    if conf.use_target_view:
        target_dim +=1
    state_with_history = np.zeros((conf.hist_len*conf.frame_frequency, conf.state_siz))

    append_to_hist(state_with_history,state,conf)
    #append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
    next_state_with_history = np.copy(state_with_history)

    #optimizer =
    train_step = optimizer.minimize(loss)
    with tf.Session() as sess:
        #init_op =
        sess.run(tf.initialize_all_variables())
        for step in xrange(conf.steps):
            if state.terminal or epi_step >= conf.early_stop:
                epi_step = 0
                nepisodes += 1
                # reset the game
                state = sim.newGame(conf.tgt_y, conf.tgt_x)
                # and reset the history
                state_with_history[:] = 0
                append_to_hist(state_with_history,state,conf)
                #append_to_hist(state_with_history, rgb2gray(state.pob).reshape(conf.state_siz))
                next_state_with_history = np.copy(state_with_history)

            epsilon = annealing_schedule(step, conf)

            if step < conf.init_rollout_length:
                action = epsilon_greedy_action(state_with_history,state,
                                           model,
                                           conf,
                                           1.0,
                                           softmax_sampling = False)
            else:
                action = epsilon_greedy_action(state_with_history,state,
                                           model,
                                           conf,
                                           epsilon,
                                           softmax_sampling = conf.do_softmax_sampling)



            action_onehot = trans.one_hot_action(action)
            next_state = sim.step(action)
            epi_step += 1

            # append to history
            #append_to_hist(next_state_with_history,
            #               rgb2gray(next_state.pob).reshape(conf.state_siz))
            append_to_hist(state_with_history,state,conf)

            sample = (select_frames_from_hist(state_with_history,state,conf),
                  action_onehot,
                  select_frames_from_hist(next_state_with_history,state,conf),
                  next_state.reward,
                  next_state.terminal)

            if conf.do_eval and (np.random.rand() < conf.val_fraction):
                # add datapoint to validation data
                trans_val.add(sample)
            else:
                if conf.minibatch_prio_sampling:
                    # add current sample to batch buffer
                    batch_buff.add(sample)
                    if (step > 0) and (step % conf.minibatch_size == 0):
                        # add batch to priority buffer
                        batch = batch_buff.get_newest_minibatch()
                        print(batch_buff.size)
                        #feed_dict = get_feed_dict(varlist,batch,0)
                        q_err = np.random.rand(conf.minibatch_size)
                        #q_err = sess.run(q_loss_single, feed_dict = feed_dict)
                        trans.add_multiple(batch, q_err)
                        del batch
                        #del feed_dict
                        del q_err
                else:
                    # add datapoint to training data
                    trans.add(sample)
            del sample

            # mark next state as current state
            state_with_history = np.copy(next_state_with_history)
            state = next_state


            if (step > conf.init_rollout_length) and np.mod(step,conf.opt_freq) == 0:

                for ii in range(conf.n_update):

                    batch  = trans.sample_minibatch()

                    feed_dict = get_feed_dict(varlist,batch,1)
                    # train network
                    #sess.run(train_step,feed_dict = feed_dict);

                    if conf.minibatch_prio_sampling:
                        #feed_dict = get_feed_dict(varlist,batch,0)
                        q_err = np.random.rand(conf.minibatch_size)
                        #q_err = sess.run(q_loss_single, feed_dict = feed_dict)
                        trans.add_multiple(batch, q_err)
                        del batch
                        #del feed_dict
                        del q_err
                    else:
                        pass



            if (conf.use_target_network
                and (step > conf.init_rollout_length)
                and np.mod(step,conf.target_update_freq) == 0):

                copyWeights(model, tmodel)
                print("Copied weights to target network")

            if conf.do_eval and (step > conf.init_rollout_length) and (np.mod(step,conf.eval_frequency) == 0):
                # compute train error
                batch = trans.sample_minibatch()
                feed_dict_trainerr = get_feed_dict(varlist,batch,0)
                train_err = sess.run(loss, feed_dict = feed_dict_trainerr)
                del batch

                # compute validation error

                batch = trans_val.sample_minibatch()
                feed_dict_evalerr = get_feed_dict(varlist,batch,0)
                val_err = sess.run(loss, feed_dict = feed_dict_evalerr)
                del batch

                #print computed error
                print(("iteration: {}, train_error: {}, validation error: {}, "
                      + "epsilon: {}").format(step, train_err, val_err, epsilon))


            if (step > 0) and (np.mod(step,conf.save_frequency) == 0):
                if conf.auto_save:
                    filename = conf.auto_save_name + "iteration{}.h5".format(step)
                    autoSaveWeights(model, filename)
                else:
                    # save dialog
                    saveWeights(model)
                    cont = raw_input("Do you want to continue training? [Y/n] ")
                    if cont.lower() == "" or cont.lower()[0] == "y":
                        pass
                    else:
                        break;

            #model.compile(loss=loss, optimizer=optim)

            #print(conf.)
            if conf.disp_on:
                if (step > conf.init_rollout_length and (np.mod(step,conf.disp_frequency) <= 200)) or step < 1000 :

                    if win_all is None:
                        plt.subplot(131)
                        win_all = plt.imshow(state.screen)
                        plt.subplot(132)
                        win_pob = plt.imshow(state.pob)
                        plt.subplot(133)
                        win_tgt_pob = plt.imshow(state.tgt_pob)
                    else:
                        win_all.set_data(state.screen)
                        win_pob.set_data(state.pob)
                        win_tgt_pob.set_data(state.tgt_pob)
                    plt.pause(conf.disp_interval)
                    plt.draw()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('config_path',help='The path to the configuartion file')
    parser.add_argument(
        '--weights',
        help='Path to pretrained weights file',
        default=None
    )
    parser.add_argument(
        '--visualize_model',
        help=('Set this FLAG to create an image of the model architecture. '
              +'When Flag is set, no training is done! '),
        action = 'store_true',
        default=False
    )
    FLAGS = parser.parse_args()
    main()