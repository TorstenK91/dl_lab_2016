import numpy as np
from random import randrange
from numpy.random import multinomial
import tensorflow as tf

import rbtree

from importlib import import_module
from os import mkdir
from os.path import abspath, join, exists, dirname, basename

from transitionTable import TransitionTable

WEIGHTS_DIR = "pretrained"

class PriorityMinibatch(TransitionTable):
    """
    This implements a dict-like structure that returns vaules based on the
    used key (Q-loss). Higher q-loss values are given higher priority, thus
    are more likely to be returned.
    """
    def __init__(self, conf):
        self.state_siz = conf.state_siz
        self.act_num = conf.act_num
        self.hist_len  = conf.hist_len
        self.batch_size = conf.minibatch_size
        self.max_transitions = conf.maxlen
        self.use_target_view = conf.use_target_view

        # memory for state transitions
        n_channels = self.hist_len
        if self.use_target_view:
            n_channels += 1
        self.n_channels = n_channels
        self.buffer = rbtree.rbtree()


    def add(self, sample, Q_loss, drop_if_minimum=False):
        """
        add a sample, Q_loss is used as key to identify this sample
        """
        if isinstance(Q_loss, np.ndarray):
            for k in range(Q_loss.size):
                qk = np.asscalar(Q_loss[k])
                sk = [s[k] for s in sample]
                self.add(sk,qk,drop_if_minimum)
            print("added {} samples. Current buffer size{}".format(len(Q_loss), len(self.buffer)))
        else:
            if len(self.buffer) < self.max_transitions:
                self.buffer[Q_loss] = sample
            else:
                Q_min = self.buffer.min()
                if drop_if_minimum and Q_loss < Q_min:
                    # would be the new minimum
                    return
                else:
                    # just replace Q_min
                    del self.buffer[Q_min]
                    self.buffer[Q_loss] = sample
        del sample
        del Q_loss

    def get_newest_minibatch(self,batch_size = None):
        msg = "Beg Tobi to implement this or do it yourself"
        raise(NotImplementedError(msg))

    def sample_minibatch(self, batch_size=None, Q_loss_priority=True, p=1e-3):
        """
        Returns a list of batch_size samples, Q_loss_priority enables priority
        given to samples with high Q-loss values
        """
        if batch_size is None:
            batch_size = self.batch_size
        if batch_size > len(self.buffer):
            raise(ValueError("Insufficient samples. Buffer size {}".format(len(self.buffer))))
        # get a list of indexes, lower values are more likely
        if Q_loss_priority:
            indexes = np.random.geometric(p, batch_size) % len(self.buffer)
        else:
            indexes = np.random.randint(0, len(self.buffer), batch_size)

        Q_values = np.array(self.buffer.keys())[indexes]

        state      = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        action     = np.zeros((self.batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        reward     = np.zeros((self.batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((self.batch_size, 1), dtype=np.float32)
        for i,q in enumerate(Q_values):
            state[i],action[i],next_state[i],reward[i],terminal[i] = self.buffer[q]

        for Q in set(Q_values):
            del self.buffer[Q]

        return state, action, next_state, reward, terminal

def test_PriorityMinibatch():
    from timeit import default_timer as timer
    from mock import MagicMock
    N = 1* 10**6
    Q_loss = np.random.rand(N) + 100.
    batches = range(N)
    n = 100
    max_len = 10**5
    default_batch_size = 10

    conf = MagicMock()
    conf.maxlen = max_len
    conf.minibatch_size = default_batch_size
    prioBuf = PriorityMinibatch(conf)
    print(prioBuf.max_transitions)

    # test filling
    print("Filling Buffer")
    start = timer()
    prioBuf.add(batches, Q_loss)
    end = timer()
    el = end-start
    el_avg = el / N
    print("Buffer filled in {} s, that's {} s per iteration"
          .format(el, el_avg))
    print("Filling Buffer over")
    start = timer()
    for k in range(max_len):
        prioBuf.add(batches[k], Q_loss[k])
    end = timer()
    el = end-start
    el_avg = el / N
    print("Buffer overwriting in {} s, that's {} s per iteration"
          .format(el, el_avg))

    # test sampling
    start = timer()
    for k in range(n):
        n_batches = max_len/n
        bb = prioBuf.sample_minibatch(n_batches)
        if len(bb) != n_batches:
            print("received wrong number of batches! Expected: {}, Got: {}"
                  .format(n_batches, len(bb)))

    end = timer()
    el = end-start
    el_avg = el / N
    print("Sampled {} times in {} s, that's {} s per iteration"
          .format(max_len,el, el_avg))
    print("Test succeeded")


class State: # return tuples made easy
    def __init__(self, action, reward, screen, terminal, pob,tgt_pob):
        self.action   = action
        self.reward   = reward
        self.screen   = screen
        self.terminal = terminal
        self.pob      = pob
        self.tgt_pob  = tgt_pob


# The following functions were taken from scikit-image
# https://github.com/scikit-image/scikit-image/blob/master/skimage/color/colorconv.py

def rgb2gray(rgb):
    if rgb.ndim == 2:
        return np.ascontiguousarray(rgb)

    gray = 0.2125 * rgb[..., 0]
    gray[:] += 0.7154 * rgb[..., 1]
    gray[:] += 0.0721 * rgb[..., 2]

    return gray


def loadModel(model_path,opt):
    """
    This function loads a model specified in model_path
    """
    model_path = abspath(model_path)
    if not exists(model_path):
        raise(ValueError("The specified model does not exist!"))
    else:
        filename = basename(model_path)
        module_name = filename.split('.')[0]
        module_name = 'models.' + module_name
        model_definition = import_module(module_name)
        return model_definition.createModel(opt)

def loadConfig(conf_path):
    conf_path = abspath(conf_path)
    if not exists(conf_path):
        raise(ValueError("The specified configuration does not exist!"))
    else:
        filename = basename(conf_path)
        module_name = filename.split('.')[0]
        module_name = 'config.' + module_name
        configuration = import_module(module_name)
        conf = configuration.Config()
        conf_dir = dirname(conf_path)
        model_path = join(conf_dir, '..', 'models', conf.model_name)
        model, optimizer = loadModel(model_path, conf)
        return model, optimizer, conf


def restoreWeights(model, weights_path=None, by_name=True):
    """
    This function loads pretrained weights specified in weights_path
    """
    if weights_path:
        weights_path = abspath(weights_path)
        if not exists(weights_path):
            raise(ValueError("The specified weights file does not exist!"))
        else:
            model.load_weights(weights_path, by_name=by_name)
            print("Weights {} restored.".format(weights_path))
    else:
        pass

    return model


def saveWeights(model):
    """
    This function saves the weights of a session into file `weights_name`.
    """

    save = raw_input("Do you want to save the current weights? [Y/n] ")
    if save and save.lower()[0] == "n":
        pass
    else:
        filename = ""
        while not filename:
            filename = raw_input("Please enter a filename: ")
        print("saving weights now...")

        weights_dir = abspath(WEIGHTS_DIR)
        if not exists(weights_dir):
            mkdir(weights_dir)
            print("created {}".format(weights_dir))
        filepath = join(weights_dir, filename)

        model.save_weights(filepath)
        print("The current weights are safely stored at {}".format(filepath))

def autoSaveWeights(model, filename):
    weights_dir = abspath(WEIGHTS_DIR)
    if not exists(weights_dir):
        mkdir(weights_dir)
        print("created {}".format(weights_dir))
    filepath = join(weights_dir, filename)
    model.save_weights(filepath)
    print("The current weights are safely stored at {}".format(filepath))

def copyWeights(sourceModel, targetModel):
    targetModel.set_weights(sourceModel.get_weights())

def reshapeData(data, opt):
    """ reshapes data from (num_datapoints, state_size) to
    (num_datapoints, hist_size, img_size, img_size)"""
    hist_size = opt.hist_len
    num_datapoints = data.shape[0]
    img_size = opt.pob_siz * opt.cub_siz
    assert(data.shape[1] == img_size**2 * hist_size)
    new_shape = [num_datapoints, hist_size, img_size, img_size]
    #print("reshaped data from: {} to: {}".format(data.shape, new_shape))
    data = data.reshape(new_shape)
    return np.rollaxis(data, 1, 4)

def reshapeSingle(data,conf):
    n_channels = conf.hist_len
    if conf.use_target_view:
        n_channels += 1
    img_size = conf.pob_siz * conf.cub_siz
    #assert (data.shape[0] == img_size**2 * hist_size)
    new_shape = [1, n_channels, img_size, img_size]
    data = data.reshape(new_shape)
    return np.transpose(data,(0,2,3,1))



def tf_reshapeData(data, conf,num_datapoints):
    """ reshapes data from (num_datapoints, state_size) to
    (num_datapoints, hist_size, img_size, img_size)"""
    #hist_size = conf.hist_len

    n_channels = conf.hist_len
    if conf.use_target_view:
        n_channels += 1

    #num_datapoints = data.shape[0]
    img_size = conf.pob_siz * conf.cub_siz
    #assert(data.shape[1] == img_size**2 * hist_size)
    new_shape = [num_datapoints, n_channels, img_size, img_size]
    #print("reshaped data from: {} to: {}".format(data.shape, new_shape))
    data = tf.reshape(data,new_shape)
    return tf.transpose(data,(0,2,3,1))

def select_frames_from_hist(hist,state,conf):
    """


    """
    if conf.frame_frequency == 1:
        hist_new = hist
    else:
        n_frames = conf.frame_frequency*conf.hist_len
        index = np.arange(n_frames)[::-1][0:n_frames:conf.frame_frequency]
        hist_new = hist[index,:]

    if conf.use_target_view:
        #print("--")
        #print(np.shape(hist_new))
        tgt = rgb2gray(state.tgt_pob).reshape(conf.state_siz)
        hist_new = np.vstack((hist_new,tgt))
        #print(np.shape(hist_new))
    return hist_new.reshape(-1)


def append_to_hist(state_with_hist,obs, conf):
    """
    Add observation to the state.


    """
    #print(obs.pob)
    new_state = rgb2gray(obs.pob).reshape(conf.state_siz)


    for i in range(state_with_hist.shape[0]-1):
        state_with_hist[i, :] = state_with_hist[i+1, :]
    state_with_hist[-1, :] = new_state

"""
def append_to_hist(state, obs):

    #Add observation to the state.



    for i in range(state.shape[0]-1):
        state[i, :] = state[i+1, :]
    state[-1, :] = obs
"""

def softmax(x):
    """Compute softmax values for each sets of scores in x.

    from http://stackoverflow.com/questions/34968722/softmax-function-python
    """
    return np.exp(x) / np.sum(np.exp(x), axis=0)


def _sample_action(softmax_output):
    """
    Sample an action from the softmax output of the CNN

    """

    softmax_norm = softmax_output / (np.sum(softmax_output) + 1e-5)
    action = multinomial(1,softmax_norm)

    return np.argmax(action)

def epsilon_greedy_action(state_with_history,state,model,conf,epsilon,softmax_sampling = False):
    """
    Sample a random action with probability epsilon, and
    the optimal action otherwise

    do_softmax_sampling:   with probability epsilon sample from the softmax output
                           of the model


    """
    hist = select_frames_from_hist(state_with_history,state,conf)
    if np.random.rand() < epsilon:
        return randrange(conf.act_num)
    else:
        if softmax_sampling:
            softout = softmax(model.predict(reshapeSingle(hist,conf)))
            return _sample_action(np.squeeze(softout))
        prediction = model.predict(reshapeSingle(hist,conf))
        return np.argmax(np.squeeze(prediction))


def annealing_schedule(step, conf):
    """
    implements the decay of epsilon over time.

    To test the behavior, just call it with a configuration and
    input different values for step, i.e. 1, conf.n_no_epsilon,
    and (conf.epsilon_decay)**-1
    """
    if step < conf.init_rollout_length:
        # this just gets a random action
        epsilon = 1.0
    elif step > conf.n_no_epsilon:
        epsilon = 0.0
    elif conf.epsilon_greedy <= conf.epsilon_min:
        epsilon = conf.epsilon_min
    else:
        epsilon = (conf.epsilon_greedy - conf.epsilon_min)
        epsilon = epsilon * np.exp(-conf.epsilon_decay*step) + conf.epsilon_min
    return epsilon

if __name__ == "__main__":
    test_PriorityMinibatch()