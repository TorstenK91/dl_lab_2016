#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 13:01:14 2017

THE supperior network for Deep Q-Learning

@author: schoelst
"""


from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers.advanced_activations import LeakyReLU

import tensorflow as tf

def createModel(conf):

    img_size = conf.pob_siz * conf.cub_siz
    n_channels =  conf.hist_len
    if conf.use_target_view:
        n_channels += 1

    model = Sequential()
    # convolutional layers
    model.add(Convolution2D(16, 5, 5, border_mode='valid', 
                            input_shape=(img_size,img_size,n_channels), 
                            subsample=(2,2)))
    model.add(Activation('relu'))
    model.add(Convolution2D(32, 3, 3, border_mode='valid'))
    model.add(Activation('relu'))

    model.add(Flatten())
    # Note: Keras does automatic shape inference.
    model.add(Dense(256))
    model.add(LeakyReLU(alpha=conf.relu_factor))
    model.add(Dropout(0.25))

    model.add(Dense(5))
    if conf.output_activation == 0:
        # linear output (-inf, inf)
        pass
    elif conf.output_activation == 1:
        # softmax activation (0,1)
        model.add(Activation('softmax'))
    elif conf.output_activation == 2:
        # softsign actiavation (-1, 1)
        model.add(Activation('softsign'))
    else:
        print("Invalid output_activation, using linear activation")
        # linear output (-inf, inf)
        pass

    optimizer = tf.train.AdamOptimizer(learning_rate=conf.init_learning_rate,
                                       beta1=0.9,
                                       beta2=0.999,
                                       epsilon=1e-08, 
                                       use_locking=False,
                                       name='Adam')

    return model, optimizer

if __name__ == '__main__':
    print("creating a model now")
    createModel()