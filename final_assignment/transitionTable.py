import numpy as np
import heapq
from random import randrange

class TransitionTable:

    # basic funcs

    def __init__(self, conf):
        self.state_siz = conf.state_siz
        self.act_num = conf.act_num
        self.hist_len  = conf.hist_len
        self.batch_size = conf.minibatch_size
        self.max_transitions = conf.maxlen
        self.use_target_view = conf.use_target_view

        # memory for state transitions
        n_channels = self.hist_len
        if self.use_target_view:
            n_channels += 1
        self.n_channels = n_channels
        self.states  = np.zeros((self.max_transitions, self.state_siz*n_channels))
        self.actions = np.zeros((self.max_transitions, self.act_num))
        self.next_states = np.zeros((self.max_transitions, self.state_siz*n_channels))
        self.rewards = np.zeros((self.max_transitions, 1))
        self.terminal = np.zeros((self.max_transitions, 1))
        self.top = 0
        self.bottom = 0
        self.size = 0

    # helper funcs
    def add(self, sample, Q_loss = None, drop_if_minimum = None):
        state, action, next_state, reward, terminal = sample
        self.states[self.top] = state
        self.actions[self.top] = action
        self.next_states[self.top] = next_state
        self.rewards[self.top] = reward
        self.terminal[self.top] = terminal
        if self.size == self.max_transitions:
            self.bottom = (self.bottom + 1) % self.max_transitions
        else:
            self.size += 1
        self.top = (self.top + 1) % self.max_transitions

    def one_hot_action(self, actions):
        actions = np.atleast_2d(actions)
        one_hot_actions = np.zeros((actions.shape[0], self.act_num))
        for i in range(len(actions)):
            one_hot_actions[i, int(actions[i])] = 1
        return one_hot_actions

    def get_newest_minibatch(self,batch_size = None):
        if batch_size is None:
            batch_size = self.batch_size
        indices = np.arange((self.top-batch_size),self.top)
        state      = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        action     = np.zeros((self.batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        reward     = np.zeros((self.batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((self.batch_size, 1), dtype=np.float32)
        for i in range(batch_size):
            index = indices[i]
            state[i]         = self.states.take(index, axis=0, mode='wrap')
            action[i]        = self.actions.take(index, axis=0, mode='wrap')
            next_state[i]    = self.next_states.take(index, axis=0, mode='wrap')
            reward[i]        = self.rewards.take(index, axis=0, mode='wrap')
            terminal[i]      = self.terminal.take(index, axis=0, mode='wrap')
        return state, action, next_state, reward, terminal

    def sample_minibatch(self, batch_size=None, Q_loss_priority=True, p=1e-3):
        if batch_size is None:
            batch_size = self.batch_size
        state      = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        action     = np.zeros((self.batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        reward     = np.zeros((self.batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((self.batch_size, 1), dtype=np.float32)
        for i in range(batch_size):
            index = np.random.randint(self.bottom, self.bottom + self.size)
            state[i]         = self.states.take(index, axis=0, mode='wrap')
            action[i]        = self.actions.take(index, axis=0, mode='wrap')
            next_state[i]    = self.next_states.take(index, axis=0, mode='wrap')
            reward[i]        = self.rewards.take(index, axis=0, mode='wrap')
            terminal[i]      = self.terminal.take(index, axis=0, mode='wrap')
        return state, action, next_state, reward, terminal


class TransitionTableRankPrio:

    def __init__(self, conf):
        self.state_siz = conf.state_siz
        self.act_num = conf.act_num
        self.hist_len  = conf.hist_len
        self.batch_size = conf.minibatch_size
        self.max_transitions = conf.maxlen
        self.minibatch_size = conf.minibatch_size
        self.n_segments = self.minibatch_size

        self.prio_len = 0
        self.priolist = []
        self.total_counter = 0

        self.use_target_view = conf.use_target_view

        # memory for state transitions
        n_channels = self.hist_len
        if self.use_target_view:
            n_channels += 1
        self.n_channels = n_channels

    def one_hot_action(self, actions):
        actions = np.atleast_2d(actions)
        one_hot_actions = np.zeros((actions.shape[0], self.act_num))
        for i in range(len(actions)):
            one_hot_actions[i, int(actions[i])] = 1
        return one_hot_actions

    def add(self, sample, q_loss):
        item = (q_loss,self.total_counter,sample)
        heapq.heappush(self.priolist,item)


        if len(self.priolist) > self.max_transitions :
                self.priolist.pop()

        self.total_counter +=1

    def add_multiple(self,Samples,Q_loss):
        """

        """

        assert(np.shape(Samples[0])[0] == len(Q_loss))
        n_samples = len(Q_loss)
        for ii in range(n_samples):
            item = (Samples[0][ii],Samples[1][ii],Samples[2][ii],
                                    Samples[3][ii],Samples[4][ii])

            heapq.heappush(self.priolist,(Q_loss[ii],self.total_counter,item))
            self.total_counter +=1

            if len(self.priolist) > self.max_transitions:
                self.priolist.pop()
                #print("pop it like its hot")
                #print(len(self.priolist))



    def sample_minibatch(self, batch_size=None, Q_loss_priority=True, p=1e-3,uniform = False):
        """

        """
        state      = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        action     = np.zeros((self.batch_size, self.act_num), dtype=np.float32)
        next_state = np.zeros((self.batch_size, self.state_siz*self.n_channels), dtype=np.float32)
        reward     = np.zeros((self.batch_size, 1), dtype=np.float32)
        terminal   = np.zeros((self.batch_size, 1), dtype=np.float32)

        if batch_size is None:
            batch_size = self.batch_size

        for ii in range(batch_size):
            if uniform:
                index = randrange(self.prio_len)
            else:

                index = np.random.geometric(p)

            q_loss,_, item = self.priolist[index]

            state[ii]         = item[0]
            action[ii]        = item[1]
            next_state[ii]    = item[2]
            reward[ii]        = item[3]
            terminal[ii]      = item[4]


            self.remove_at_index(index)

        return state,action,next_state,reward,terminal



    def remove_at_index(self,i):
        """
        This is taken from:
        http://stackoverflow.com/questions/10162679/python-delete-element-from-heap

        We just hope this works <3
        """
        self.priolist[i] = self.priolist[-1] # overwrite i-th element with last element
        self.priolist.pop() #delete the last element (therefore also the one at i)
        heapq._siftup(self.priolist, i) #heaoify up
        heapq._siftdown(self.priolist, 0, i)# heapify down












    # basic funcs






